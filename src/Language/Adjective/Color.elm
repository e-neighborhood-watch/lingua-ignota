module Language.Adjective.Color exposing
  ( Color (..)
  , Shade (..)
  , PrimaryColor (..)
  )

type PrimaryColor
  = Red
  | Blue
  | Green

type Shade
  = Light
  | Dark
  | Neutral

type Color
  = Nocolor Shade
  | PrimaryColor Shade PrimaryColor
  | SecondaryColor Shade PrimaryColor PrimaryColor
  -- In tertiary color the first argument is the dominant color
  -- e.g. TertiaryColor Red Green is orange
  | TertiaryColor Shade PrimaryColor PrimaryColor
