module Language.Marking.Verb exposing
  ( VerbMarking (..)
  , generate
  )

import Language.Fixing as Fixing exposing
  ( Fixing
  )

import Language.SentencePosition as SentencePosition exposing
  ( SentencePosition
  )

import Random

type VerbMarking
  = VerbMarking SentencePosition Fixing

generate : Random.Generator VerbMarking
generate =
  Random.map2
    VerbMarking
    ( Random.uniform
      SentencePosition.Sentence
      [ SentencePosition.Verb
      ]
    )
    Fixing.generate
