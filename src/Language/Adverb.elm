module Language.Adverb exposing
  ( Adverb (..)
  , translate
  )

import Language.Decode as Decode

type Adverb
  = Tonight

translate : { c | reverseDictionary : Int -> List b } -> Adverb -> List b
translate =
  Decode.Class
    { classIndex =
      10
    , decoder =
      ( \ adverb ->
        case
          adverb
        of
          Tonight ->
            0
      )
    }
  |> Decode.translate
