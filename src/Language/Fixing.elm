module Language.Fixing exposing
  ( Fixing (..)
  , apply
  , generate
  )

import Random

type Fixing
  = Pre
  | Post

apply : Fixing -> phonemes -> List phonemes -> List phonemes
apply fixing fix rest =
  case
    fixing
  of
    Pre ->
      fix :: rest
    Post ->
      rest
      ++ List.singleton fix

generate : Random.Generator Fixing
generate =
  Random.uniform
    Pre
    [ Post
    ]
