module Language.GrammaticalNumber exposing
  ( GrammaticalNumber (..)
  , Marking
  , generate
  )

import Extra.Random as Random

import Random

type GrammaticalNumber
  = None
  | SingularPlural Bool Marking

type Marking
  = All
  | NonSingular
  | NonPlural

generate : Random.Generator GrammaticalNumber
generate =
  ( Random.uniform
    ( always
      None
    )
    [ SingularPlural True
    , SingularPlural False
    ]
  )
  |>
    Random.apTo
      ( Random.uniform
        All
        [ NonSingular
        , NonPlural
        ]
      )
