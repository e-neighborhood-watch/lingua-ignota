module Language.SubjectComplement exposing
  ( SubjectComplement (..)
  , translate
  )

import Language exposing
  ( Language
  )
import Language.Adjective as Adjective exposing
  ( Adjective
  )
import Language.NounPhrasal as NounPhrasal exposing
  ( NounPhrasal
  )
import Language.Phoneme as Phoneme exposing
  ( Phoneme
  )

type SubjectComplement
  = NounPhrase NounPhrasal
  | AdjectivePhrase Adjective

translate : Language -> SubjectComplement -> List (List Phoneme)
translate lang subjComp =
  case
    subjComp
  of
    NounPhrase nounPhrase ->
      NounPhrasal.translate lang nounPhrase
    AdjectivePhrase adjPhrase ->
      Adjective.translate lang adjPhrase
      |> List.singleton
