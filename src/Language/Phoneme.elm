module Language.Phoneme exposing
  ( Phoneme (..)
  , Voicing (..)
  , ArticulationPoint (..)
  , Closeness (..)
  , Backness (..)
  , Rounding (..)
  , Length (..)
  , Lateralization (..)
  , Labialization (..)
  , Aspiration (..)
  , RawConsonant (..)
  , xsampa
  )

type Voicing
  = Voiceless
  | Voiced


type Closeness
  = Open
  | OpenMid
  | Mid
  | CloseMid
  | Close


type Backness
  = Front
  | Central
  | Back


type Rounding
  = Rounded
  | Unrounded


type Length
  = Short
  | Long


type ArticulationPoint
  = Bilabial
  | Labiodental
  | Dental
  | Alveolar
  | PostAlveolar
  | Palatal
  | Velar
  | Uvular
  | Epiglottal
  | Glottal


type Lateralization
  = Centralized
  | Lateralized


type Aspiration
  = Aspirated
  | Unaspirated


type RawConsonant
  = Nasal ArticulationPoint
  | Plosive Aspiration ArticulationPoint
  | Fricative Lateralization ArticulationPoint
  | Approximant Lateralization ArticulationPoint
  | Tap ArticulationPoint
  | Trill ArticulationPoint
  | SibilantAffricate ArticulationPoint


type Labialization
  = Labialized
  | Unlabialized


type Phoneme
  = Vowel Rounding Backness Closeness Length
  | Consonant Voicing Labialization RawConsonant


-- This is not complete!
-- Do not use this unless you know your symbol is here!
xsampa : String -> Phoneme
xsampa string =
  case
    List.reverse (String.toList string)
  of
    [ 'p' ] ->
      Consonant Voiceless Unlabialized (Plosive Unaspirated Bilabial)
    [ 'k' ] ->
      Consonant Voiceless Unlabialized (Plosive Unaspirated Velar)
    [ 't' ] ->
      Consonant Voiceless Unlabialized (Plosive Unaspirated Alveolar)
    [ '\\', 's', '_', 't' ] ->
      Consonant Voiceless Unlabialized (SibilantAffricate PostAlveolar)
    [ 'b' ] ->
      Consonant Voiced Unlabialized (Plosive Unaspirated Bilabial)
    [ 'g' ] ->
      Consonant Voiced Unlabialized (Plosive Unaspirated Velar)
    [ 'd' ] ->
      Consonant Voiced Unlabialized (Plosive Unaspirated Alveolar)
    [ '?' ] ->
      Consonant Voiceless Unlabialized (Plosive Unaspirated Glottal)
    [ 'h' ] ->
      Consonant Voiceless Unlabialized (Fricative Centralized Glottal)
    [ '4' ] ->
      Consonant Voiced Unlabialized (Tap Alveolar)
    [ 'r' ] ->
      Consonant Voiced Unlabialized (Trill Alveolar)
    [ 'w' ] ->
      Consonant Voiced Labialized (Approximant Centralized Velar)
    [ '\\', 'M' ] ->
      Consonant Voiced Unlabialized (Approximant Centralized Velar)
    [ 'j' ] ->
      Consonant Voiced Unlabialized (Approximant Centralized Palatal)
    [ 'm' ] ->
      Consonant Voiced Unlabialized (Nasal Bilabial)
    [ 'n' ] ->
      Consonant Voiced Unlabialized (Nasal Alveolar)
    [ 'N' ] ->
      Consonant Voiced Unlabialized (Nasal Velar)
    [ 'S' ] ->
      Consonant Voiceless Unlabialized (Fricative Centralized PostAlveolar)
    [ 'Z' ] ->
      Consonant Voiced Unlabialized (Fricative Centralized PostAlveolar)
    [ 's' ] ->
      Consonant Voiceless Unlabialized (Fricative Centralized Alveolar)
    [ 'z' ] ->
      Consonant Voiced Unlabialized (Fricative Centralized Alveolar)
    [ 'x' ] ->
      Consonant Voiceless Unlabialized (Fricative Centralized Velar)
    [ 'G' ] ->
      Consonant Voiced Unlabialized (Fricative Centralized Velar)
    [ 'f' ] ->
      Consonant Voiceless Unlabialized (Fricative Centralized Labiodental)
    [ 'v' ] ->
      Consonant Voiced Unlabialized (Fricative Centralized Labiodental)
    [ '\\', 'p' ] ->
      Consonant Voiceless Unlabialized (Fricative Centralized Bilabial)
    [ 'B' ] ->
      Consonant Voiced Unlabialized (Fricative Centralized Bilabial)
    [ 'l' ] ->
      Consonant Voiced Unlabialized (Approximant Lateralized Alveolar)
    [ 'T' ] ->
      Consonant Voiceless Unlabialized (Fricative Centralized Dental)
    [ 'D' ] ->
      Consonant Voiced Unlabialized (Fricative Centralized Dental)
    [ 'a' ] ->
      Vowel Unrounded Front Open Short
    [ 'e' ] ->
      Vowel Unrounded Front CloseMid Short
    [ 'i' ] ->
      Vowel Unrounded Front Close Short
    [ 'o' ] ->
      Vowel Rounded Back CloseMid Short
    [ 'u' ] ->
      Vowel Rounded Back Close Short
    [ 'y' ] ->
      Vowel Rounded Front Close Short
    [ '@' ] ->
      Vowel Unrounded Central Mid Short
    [ '9' ] ->
      Vowel Rounded Front Mid Short
    ':' :: x ->
      case
        x |> List.reverse |> String.fromList |> xsampa
      of
        -- : for long only works on simple vowels right now!
        Vowel rnd bck cls lng ->
          Vowel rnd bck cls Long
        otr ->
          otr
    'h' :: '_' :: x ->
      case
        x |> List.reverse |> String.fromList |> xsampa
      of
        Consonant vce lbl (Plosive asp art) ->
          Consonant vce lbl (Plosive Aspirated art)
        otr ->
          otr
    'w' :: '_' :: x ->
      case
        xsampa (String.fromList x)
      of
        Consonant vce lbl cnt ->
          Consonant vce Labialized cnt
        Vowel rnd bck cls lng ->
          Vowel Rounded bck cls lng
    'v' :: '_' :: x ->
      case
        x |> List.reverse |> String.fromList |> xsampa
      of
        Consonant vce lbl cnt ->
          Consonant Voiced lbl cnt
        vwl ->
          vwl
    '0' :: '_' :: x ->
      case
        x |> List.reverse |> String.fromList |> xsampa
      of
        Consonant vce lbl cnt ->
          Consonant Voiceless lbl cnt
        vwl ->
          vwl
    _ ->
      xsampa "@"
