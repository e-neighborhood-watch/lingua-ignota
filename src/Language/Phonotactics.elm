module Language.Phonotactics
  exposing
    ( Weight
    , Phonotactics
    , State (..)
    , generate
    )


import Extra.Random as Random
import Language.Phoneme as Phoneme
  exposing
    ( Phoneme
    )
import Language.Phoneme.Inventory.Polynesian as PolynesianPhonemicInventory
import Lazy
  exposing
    ( Lazy
    )


import Random


type alias Weight =
  Int


type alias Phonotactics =
  { compounder :
    List Phoneme -> List Phoneme -> List Phoneme
  , markovModel :
    State
  }


type alias Transition =
  { weight :
    Weight
  , to :
    Lazy State
  , phoneme :
    List Phoneme
  }


type State
  = State
    { transitions :
      List Transition
    }


type alias SyllableStructure =
  List (List (List Phoneme))


endState : Lazy State
endState () =
  State
    { transitions =
      [ ]
    }


generateFromSyllableStructure : List SyllableStructure -> Phonotactics
generateFromSyllableStructure sStructures =
  let
    go : Int -> List SyllableStructure -> Lazy State
    go n syllableStructures () =
      State
        { transitions =
          List.map
            ( \ syllableStructure ->
              ( Transition
                2
                (formFromSyllableStructure n syllableStructure)
                []
              )
            )
            syllableStructures
        }


    formFromSyllableStructure : Int -> SyllableStructure -> Lazy State
    formFromSyllableStructure n syllableStructure =
      case
        syllableStructure
      of
        [] ->
          syllable n
        (clusters :: tail) ->
          State
            { transitions =
              List.map
                (Transition 2 (formFromSyllableStructure n tail))
                clusters
            }
          |> always


    syllable : Int -> Lazy State
    syllable n () =
      State
        { transitions =
          [ { to =
              go (n - 1) sStructures
            , phoneme =
              [
              ]
            , weight =
              2 * n
            }
          , { to =
              endState
            , phoneme =
              [
              ]
            , weight =
              1
            }
          ]
        }

  in
    { markovModel =
      go 1 sStructures ()
    , compounder =
      simpleCompounder
    }


-- Combines two syllables eliminating double letters at the boundary
simpleCompounder : List Phoneme -> List Phoneme -> List Phoneme
simpleCompounder leading following =
  case
    following
  of
    [] ->
      leading
    (firstB :: restB) ->
      case
        List.reverse leading
      of
        [] ->
          following
        (lastA :: _) ->
          if
            lastA == firstB
          then
            leading ++ restB
          else
            leading ++ following


generate : Random.Generator Phonotactics
generate =
  PolynesianPhonemicInventory.generate
  |> Random.map
     ( \ { nasals
         , plosives
         , liquids
         , miscConsonants
         , monophthongs
         , dipthongs
         } ->
       Random.uniform
       -- CV(N)
       ( generateFromSyllableStructure
         [ [ nasals ++ plosives ++ liquids ++ miscConsonants
           , monophthongs ++ monophthongs ++ dipthongs
           , nasals
           ]
         , [ nasals ++ plosives ++ liquids ++ miscConsonants
           , monophthongs ++ monophthongs ++ dipthongs
           ]
         ]
       )
       -- CV
       [ generateFromSyllableStructure
         [ [ nasals ++ plosives ++ liquids ++ miscConsonants
           , monophthongs ++ monophthongs ++ dipthongs
           ]
         ]
       ]
     )
  |> Random.join
