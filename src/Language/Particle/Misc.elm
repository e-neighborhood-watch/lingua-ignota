module Language.Particle.Misc exposing
  ( MiscParticle (..)
  , translate
  )

import Language.Decode as Decode

type MiscParticle
  = Composition
  | Conditional

translate : { a | reverseDictionary : Int -> List b} -> MiscParticle -> List b
translate =
  Decode.Class
    { classIndex =
      11
    , decoder =
      ( \ particle ->
        case
          particle
        of
          Composition ->
            0
          Conditional ->
            1
      )
    }
  |> Decode.translate

