module Language.Particle.Number exposing
  ( NumberParticle (..)
  , translate
  , decoder
  , normalize
  , total
  )

import Language exposing
  ( Language
  )
import Language.Decode as Decode
import Language.GrammaticalNumber as GrammaticalNumber
import Language.Phoneme exposing
  ( Phoneme
  )

type NumberParticle
  = Singular
  | Dual
  | Plural

total : Int
total =
  3

decoder : Decode.Class NumberParticle
decoder =
  Decode.Class
    { classIndex =
      5
    , decoder =
      ( \ particle ->
        case
          particle
        of
          Singular ->
            0
          Dual ->
            2
          Plural ->
            3
      )
    }

translate : Language -> NumberParticle -> List Phoneme
translate language =
  normalize language
  >> Decode.translate decoder language

normalize : Language -> NumberParticle -> NumberParticle
normalize { grammaticalNumber } np =
  case
    grammaticalNumber
  of
    GrammaticalNumber.SingularPlural dual _ ->
      case
        np
      of
        Dual ->
          Plural
        _ ->
          np
    GrammaticalNumber.None ->
      Plural
