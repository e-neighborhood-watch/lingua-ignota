module Language.Conjunction exposing
  ( Conjunction (..)
  , translate
  )

import Language exposing
  ( Language
  )
import Language.Decode as Decode
import Language.Phoneme exposing
  ( Phoneme
  )

type Conjunction
  = Illative
    -- e.g. 'for', 'because'
  | Cumulative
    -- e.g. 'and'
  | Adversative
    -- e.g. 'but', 'however'
  | Alternative
    -- e.g. 'or'

translate : Language -> Conjunction -> List Phoneme
translate =
  Decode.Class
    { classIndex =
      9
    , decoder =
      ( \ conjunct ->
        case
          conjunct
        of
          Illative ->
            0
          Cumulative ->
            1
          Adversative ->
            2
          Alternative ->
            3
      )
    }
  |> Decode.translate
