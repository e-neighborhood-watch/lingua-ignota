module Language.Ontology
  exposing
    ( Ontology
    )

type alias Ontology a =
  a -> List (List a)
