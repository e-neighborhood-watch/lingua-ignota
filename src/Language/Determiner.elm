module Language.Determiner exposing
  ( Determiner (..)
  , translate
  )

import Language
  exposing
    ( Language
    )
import Language.Decode as Decode
import Language.GrammaticalNumber as GrammaticalNumber
import Language.Particle.Number as Number
  exposing
    ( NumberParticle
    )
import Language.Phoneme as Phoneme
  exposing
    ( Phoneme
    )
import Language.Pronoun.Personal as PersonalPronoun
  exposing
    ( PersonalPronoun
    )


type Determiner
  = Article Bool NumberParticle
  | Negative
  | PossessivePronoun PersonalPronoun NumberParticle


decoder : Decode.Class Determiner
decoder =
  Decode.Class
    { classIndex =
      10
    , decoder =
      ( \ prp ->
        case
          prp
        of
          Article True numberParticle ->
            ( Decode.decode Number.decoder numberParticle
            * 4
            )
          Article False numberParticle ->
            ( Decode.decode Number.decoder numberParticle
            * 4
            + 1
            )
          PossessivePronoun personalPronoun numberParicle ->
            ( ( Number.total -- This must be the number of number particles
              * Decode.decode PersonalPronoun.decoder personalPronoun
              + Decode.decode Number.decoder numberParicle
              )
            * 4
            + 2
            )
          Negative ->
            3
      )
    }


normalize : Language -> Determiner -> Determiner
normalize language determiner =
  case
    determiner
  of
    Negative ->
      Negative
    Article definiteness numberParticle ->
      numberParticle
      |> Number.normalize language
      |> Article (definiteness && language.definiteIndefiniteDistinction)
    PossessivePronoun pp numberParticle ->
      pp
      |> PersonalPronoun.normalize language
      |> PossessivePronoun
      |> (|>) (Number.normalize language numberParticle)


translate : Language -> Determiner -> List (List Phoneme)
translate language det =
  case
    ( det
    , ( language.grammaticalNumber
      , language.definiteIndefiniteDistinction
      )
    , language.splitArticles
    )
  of
    ( Article _ _, (GrammaticalNumber.None, False), _) ->
      []
    ( Article _ _, (GrammaticalNumber.None, True), _) ->
      det
      |> normalize language
      |> Decode.translate decoder language
      |> List.singleton
    ( Article determined number, (_, True), True) ->
      [ Article determined Number.Singular
        |> Decode.translate decoder language
      , Number.translate language number
      ]
    _ ->
      det
      |> normalize language
      |> Decode.translate decoder language
      |> List.singleton
