module Language.SentenceStructure exposing
  ( SentenceStructure (..)
  , generate
  , createOrder
  )

import Random

type SentenceStructure
  = IOSV
  | IOVS
  | ISOV
  | ISVO
  | IVOS
  | IVSO
  | OISV
  | OIVS
  | OSIV
  | OSVI
  | OVIS
  | OVSI
  | SIOV
  | SIVO
  | SOIV
  | SOVI
  | SVIO
  | SVOI
  | VSIO
  | VSOI
  | VIOS
  | VISO
  | VOIS
  | VOSI

generate : Random.Generator SentenceStructure
generate =
  Random.uniform
    IOSV
    [ IOVS
    , ISOV
    , ISVO
    , IVOS
    , IVSO
    , OISV
    , OIVS
    , OSIV
    , OSVI
    , OVIS
    , OVSI
    , SIOV
    , SIVO
    , SOIV
    , SOVI
    , SVIO
    , SVOI
    , VSIO
    , VSOI
    , VIOS
    , VISO
    , VOIS
    , VOSI
    ]

createOrder :
  SentenceStructure
  ->
    { verb :
      List phonemes
    , subject :
      List phonemes
    , directObject :
      List phonemes
    , indirectObject :
      List phonemes
    }
  -> List phonemes
createOrder sentenceStructure { verb, subject, directObject, indirectObject } =
  List.concat
    ( case
        sentenceStructure
      of
        IOSV ->
          [ indirectObject
          , directObject
          , subject
          , verb
          ]
        IOVS ->
          [ indirectObject
          , directObject
          , verb
          , subject
          ]
        ISOV ->
          [ indirectObject
          , subject
          , directObject
          , verb
          ]
        ISVO ->
          [ indirectObject
          , subject
          , verb
          , directObject
          ]
        IVOS ->
          [ indirectObject
          , verb
          , directObject
          , subject
          ]
        IVSO ->
          [ indirectObject
          , verb
          , subject
          , directObject
          ]
        OISV ->
          [ directObject
          , indirectObject
          , subject
          , verb
          ]
        OIVS ->
          [ directObject
          , indirectObject
          , verb
          , subject
          ]
        OSIV ->
          [ directObject
          , subject
          , indirectObject
          , verb
          ]
        OSVI ->
          [ directObject
          , subject
          , verb
          , indirectObject
          ]
        OVIS ->
          [ directObject
          , verb
          , indirectObject
          , subject
          ]
        OVSI ->
          [ directObject
          , verb
          , subject
          , indirectObject
          ]
        SIOV ->
          [ subject
          , indirectObject
          , directObject
          , verb
          ]
        SIVO ->
          [ subject
          , indirectObject
          , verb
          , directObject
          ]
        SOIV ->
          [ subject
          , directObject
          , indirectObject
          , verb
          ]
        SOVI ->
          [ subject
          , directObject
          , verb
          , indirectObject
          ]
        SVIO ->
          [ subject
          , verb
          , indirectObject
          , directObject
          ]
        SVOI ->
          [ subject
          , verb
          , directObject
          , indirectObject
          ]
        VSIO ->
          [ verb
          , subject
          , indirectObject
          , directObject
          ]
        VSOI ->
          [ verb
          , subject
          , directObject
          , indirectObject
          ]
        VIOS ->
          [ verb
          , indirectObject
          , directObject
          , subject
          ]
        VISO ->
          [ verb
          , indirectObject
          , subject
          , directObject
          ]
        VOIS ->
          [ verb
          , directObject
          , indirectObject
          , subject
          ]
        VOSI ->
          [ verb
          , directObject
          , subject
          , indirectObject
          ]
    )
