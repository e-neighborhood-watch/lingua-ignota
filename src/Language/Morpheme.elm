module Language.Morpheme exposing
  ( generate
  )

import Language.Phonotactics as Phonotactics
import Language.Phoneme as Phoneme exposing
  ( Phoneme
  )
import Lazy exposing
  ( Lazy
  )

import Random

weightSum : List { a | weight : Phonotactics.Weight } -> Phonotactics.Weight
weightSum =
  List.map .weight >> List.sum

generate : Phonotactics.State -> Random.Generator (List Phoneme)
generate (Phonotactics.State { transitions }) =
  let
    garbo : List { to : Lazy Phonotactics.State, phoneme : List Phoneme, weight : Phonotactics.Weight } -> Phonotactics.Weight -> Random.Generator (List Phoneme)
    garbo transitionList index =
      case
        transitionList
      of
        [ ] ->
          Random.constant [ ]
        { to, phoneme, weight } :: xs ->
          if
            weight > index
          then
            (( ()
            |> to
            |> generate
            |> Random.map
               (( phoneme
               |> (++)
               ))
            ))
          else
            ((( index
              - weight
              )
            |> garbo xs
            ))
  in
    case
      weightSum transitions
    of
      0 ->
        Random.constant [ ]
      totalWeight ->
        (( Random.int 0 (totalWeight - 1)
        |> Random.andThen (garbo transitions)
        ))
