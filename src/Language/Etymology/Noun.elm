module Language.Etymology.Noun
  exposing
    ( Etymology (..)
    , translate
    , nonReduplicative
    )


import Language
  exposing
    ( Language
    )
import Language.Adjective as Adjective
  exposing
    ( Adjective
    )
import Language.Basic.Noun as Noun
  exposing
    ( Noun
    )
import Language.Fixing as Fixing
import Language.Etymology.Particles as EtymologicalParticle
import Language.Phoneme as Phoneme
  exposing
    ( Phoneme
    )
import Language.Basic.Verb.Transitive as TransitiveVerb
  exposing
    ( TransitiveVerb
    )


type Etymology
  = Compound Noun Noun
  | Reduplication Noun
  | Modified Adjective Etymology
  | AgentNoun TransitiveVerb
  | PlaceOf TransitiveVerb
  | Inherit Noun


nonReduplicative : Etymology -> Bool
nonReduplicative ety =
  case
    ety
  of
    Reduplication _ ->
      False
    _ ->
      True

translate : Language -> Etymology -> List Phoneme
translate lang ety =
  case
    ety
  of
    Compound noun1 noun2 ->
      lang.phonotactics.compounder
        ( Noun.protoTranslate lang noun1 )
        ( Noun.protoTranslate lang noun2 )

    Reduplication noun ->
      lang.phonotactics.compounder
        ( Noun.protoTranslate lang noun )
        ( Noun.protoTranslate lang noun )

    Modified adj ety2 ->
      case
        lang.adjectiveFixing
      of
        Fixing.Pre ->
          lang.phonotactics.compounder
            ( Adjective.translate lang adj )
            ( translate lang ety2 )
        Fixing.Post ->
          lang.phonotactics.compounder
            ( translate lang ety2 )
            ( Adjective.translate lang adj )

    AgentNoun tVerb ->
      lang.phonotactics.compounder
        ( TransitiveVerb.protoTranslate lang tVerb )
        ( EtymologicalParticle.translate lang EtymologicalParticle.AgentNoun)
    PlaceOf tVerb ->
      lang.phonotactics.compounder
        ( TransitiveVerb.protoTranslate lang tVerb )
        ( EtymologicalParticle.translate lang EtymologicalParticle.PlaceOf)

    Inherit noun ->
      Noun.protoTranslate lang noun
