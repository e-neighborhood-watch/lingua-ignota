module Language.Etymology.Verb.Transitive exposing
  ( Etymology (..)
  , translate
  )

import Language exposing
  ( Language
  )
import Language.Basic.Verb.Transitive as TransitiveVerb exposing
  ( TransitiveVerb
  )
import Language.Phoneme exposing
  ( Phoneme
  )


type Etymology
  = Inherit TransitiveVerb


translate : Language -> Etymology -> List Phoneme
translate lang ety =
  case
    ety
  of
    Inherit verb ->
      TransitiveVerb.protoTranslate lang verb
