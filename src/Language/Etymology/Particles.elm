module Language.Etymology.Particles exposing
  ( EtymologicalParticle (..)
  , translate
  )

import Language.Decode as Decode

type EtymologicalParticle
  -- Marks an agent noun
  -- e.g. Kill + er -> Killer
  = AgentNoun
  -- Marks the place of
  -- e.g. Bake + ery -> Bakery
  | PlaceOf

translate : { a | reverseDictionary : Int -> List b} -> EtymologicalParticle -> List b
translate =
  Decode.Class
    { classIndex =
      13
    , decoder =
      ( \ particle ->
        case
          particle
        of
          AgentNoun ->
            0
          PlaceOf ->
            1
      )
    }
    |> Decode.translate
