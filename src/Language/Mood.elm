module Language.Mood exposing
  ( Mood (..)
  , translate
  )

import Language.Decode as Decode

type Mood
  = Indicative
  | Imperative
  | Prohibitive
  | Interogative
  | Conditional

translate : { a | reverseDictionary : Int -> List b } -> Mood -> List b
translate =
  Decode.Class
    { classIndex =
      6
    , decoder =
      \ miscParticle ->
        case
          miscParticle
        of
          Imperative ->
            0
          Prohibitive ->
            1
          Interogative ->
            2
          Indicative ->
            3
          Conditional ->
            4
    }
  |> Decode.translate
