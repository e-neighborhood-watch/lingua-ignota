module Language.Basic.Noun exposing
  ( Noun (..)
  , protoTranslate
  , decoder
  )


import Language
  exposing
    ( Language
    )
import Language.Decode as Decode
import Language.Phoneme
  exposing
    ( Phoneme
    )


type Noun
  = Sword
  | Book
  | Cave
  | Mushroom
  | Druid
  | Egg
  | Rock
  | Mouth
  | Flower
  | Person
  | Thing
  | Hand
  | Home
  | Water
  | Apple
  | ApplePie
  | Pie
  | Raspberry
  | Thorn
  | Fruit
  | Stew
  | Forrest
  | Orchard
  | Tree
  | Place
  | Baker
  | Bakery
  | Bread
  | Brewery


protoTranslate : Language -> Noun -> List Phoneme
protoTranslate =
  Decode.translate decoder


decoder : Decode.Class Noun
decoder =
  Decode.Class
    { classIndex =
      0
    , decoder =
      ( \ noun ->
        case
          noun
        of
          Sword ->
            0
          Book ->
            1
          Cave ->
            2
          Mushroom ->
            3
          Druid ->
            4
          Egg ->
            5
          Rock ->
            6
          Mouth ->
            7
          Flower ->
            8
          Person ->
            9
          Thing ->
            10
          Hand ->
            11
          Home ->
            12
          Water ->
            13
          Apple ->
            14
          Pie ->
            15
          Raspberry ->
            16
          Thorn ->
            17
          Fruit ->
            18
          Stew ->
            19
          Forrest ->
            20
          Orchard ->
            21
          Tree ->
            22
          Place ->
            23
          Baker ->
            24
          Bakery ->
            25
          Bread ->
            26
          Brewery ->
            27
          ApplePie ->
            28
      )
    }

