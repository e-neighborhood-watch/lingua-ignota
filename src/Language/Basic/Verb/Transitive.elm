module Language.Basic.Verb.Transitive exposing
  ( TransitiveVerb (..)
  , protoTranslate
  , decoder
  )

import Language exposing
  ( Language
  )
import Language.Decode as Decode
import Language.Phoneme exposing
 ( Phoneme
 )

type TransitiveVerb
  = Kill
  | Eat
  | Retrive
  | Use
  | Want
  | Find
  | Need
  | Cook
  | Bake
  | Brew
  | Pick
  | PickUp
  | GoTo

protoTranslate : Language -> TransitiveVerb -> List Phoneme
protoTranslate =
  Decode.translate decoder

decoder : Decode.Class TransitiveVerb
decoder =
  Decode.Class
    { classIndex =
      2
    , decoder =
      ( \ word ->
        case
          word
        of
          Kill ->
            0
          Eat ->
            1
          Retrive ->
            2
          Use ->
            3
          Want ->
            4
          Find ->
            5
          Need ->
            6
          Cook ->
            7
          Pick ->
            8
          PickUp ->
            9
          GoTo ->
            10
          Bake ->
            11
          Brew ->
            12
      )
    }
