module Language.Adjective exposing
  ( Adjective (..)
  , translate
  )

import Language.Decode as Decode

type Adjective
  = Big
  | Small
  | Dead
  | Poisonous
  | Inside
  | Outside


translate : { a | reverseDictionary : Int -> List b } -> Adjective -> List b
translate =
  Decode.translate decoder


decoder : Decode.Class Adjective
decoder =
  Decode.Class
    { classIndex =
      7
    , decoder =
      ( \ adj ->
        case
          adj
        of
          Big ->
            0
          Small ->
            1
          Dead ->
            2
          Poisonous ->
            3
          Inside ->
            4
          Outside ->
            5
      )
    }
