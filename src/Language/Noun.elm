module Language.Noun
  exposing
    ( translate
    )


import Language
  exposing
    ( Language
    )
import Language.Adjective as Adjective
import Language.Decode as Decode
import Language.Etymology.Noun as Etymology
  exposing
    ( Etymology
    )
import Language.Basic.Noun as Noun
  exposing
    ( Noun (..)
    )
import Language.Phoneme
  exposing
    ( Phoneme
    )
import Language.Basic.Verb.Transitive as TransitiveVerb


import Random


type alias Ontology =
  Noun -> List Etymology


ontology : Ontology
ontology noun =
  case
    noun
  of
    Sword ->
      [ Etymology.Modified
        Adjective.Big
        ( Etymology.Inherit
          Thorn
        )
      ]
    Book ->
      [
      ]
    Cave ->
      [ Etymology.Compound
        Rock
        Mouth
      ]
    Mushroom ->
      [ Etymology.Compound
        Druid
        Flower
      , Etymology.Compound
        Cave
        Flower
      , Etymology.Compound
        Rock
        Flower
      , Etymology.Compound
        Egg
        Flower
      ]
    Druid ->
      [ Etymology.Compound
        Flower
        Person
      , Etymology.Compound
        Mushroom
        Person
      , Etymology.Compound
        Forrest
        Person
      , Etymology.Compound
        Tree
        Person
      ]
    Egg ->
      [
      ]
    Rock ->
      [
      ]
    Flower ->
      [ Etymology.Compound
        Druid
        Mushroom
      , Etymology.Modified
        Adjective.Small
        ( Etymology.Inherit
          Tree
        )
      ]
    Mouth ->
      [
      ]
    Person ->
      [
      ]
    Thing ->
      [
      ]
    Hand ->
      [
      ]
    Home ->
      [
      ]
    Water ->
      [
      ]
    Apple ->
      [ Etymology.Compound
        Flower
        Fruit
      ]
    Pie ->
      [
      ]
    Raspberry ->
      [ Etymology.Compound
        Thorn
        Fruit
      , Etymology.Modified
        Adjective.Small
        ( Etymology.Compound
          Thorn
          Fruit
        )
      ]
    Thorn ->
      [
      ]
    Fruit ->
      [
      ]
    Stew ->
      [
      ]
    Forrest ->
      [ Etymology.Reduplication
        Tree
      , Etymology.Compound
        Tree
        Place
      ]
    Tree ->
      [
      ]
    Orchard ->
      [ Etymology.Compound
        Apple
        Forrest
      , Etymology.Compound
        Fruit
        Forrest
      , Etymology.Reduplication
        Apple
      , Etymology.Reduplication
        Fruit
      , Etymology.Compound
        Apple
        Place
      , Etymology.Compound
        Fruit
        Place
      , Etymology.PlaceOf
        TransitiveVerb.Pick
      ]
    Place ->
      [
      ]
    Baker ->
      [ Etymology.AgentNoun
        TransitiveVerb.Bake
      , Etymology.Compound
        Bread
        Person
      ]
    Bakery ->
      [ Etymology.PlaceOf
        TransitiveVerb.Bake
      , Etymology.Compound
        Bread
        Place
      ]
    Bread ->
      [
      ]
    Brewery ->
      [ Etymology.PlaceOf
        TransitiveVerb.Brew
      ]
    ApplePie ->
      [ Etymology.Compound
        Apple
        Pie
      , Etymology.Compound
        Apple
        Bread
      ]


translate : Language -> Noun -> List Phoneme
translate lang noun =
  lang.etymologySeed
  + Decode.decode Noun.decoder noun
  |> Random.initialSeed
  |>
    (( noun
    |> ontology
    |> ( if
           lang.reduplicatingEtymology
         then
           identity
         else
           List.filter Etymology.nonReduplicative
       )
    |> Random.uniform (Etymology.Inherit noun)
    |> Random.step
    ))
  |> Tuple.first
  |> Etymology.translate lang
