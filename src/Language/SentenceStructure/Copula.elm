module Language.SentenceStructure.Copula exposing
  ( CopulaStructure
  , generate
  , createOrder
  )

import Random

type CopulaStructure
  = OSV
  | OVS
  | SOV
  | SVO
  | VOS
  | VSO

generate : Random.Generator CopulaStructure
generate =
  Random.uniform
    OSV
    [ OVS
    , SOV
    , SVO
    , VOS
    , VSO
    ]

createOrder : CopulaStructure -> List phonemes -> List phonemes -> List phonemes -> List phonemes
createOrder copulaStructure cop subj comp =
  List.concat
    ( case
        copulaStructure
      of
        OSV ->
          [ comp
          , subj
          , cop
          ]
        OVS ->
          [ comp
          , cop
          , subj
          ]
        SOV ->
          [ subj
          , comp
          , cop
          ]
        SVO ->
          [ subj
          , cop
          , comp
          ]
        VOS ->
          [ cop
          , comp
          , subj
          ]
        VSO ->
          [ cop
          , subj
          , comp
          ]
    )
