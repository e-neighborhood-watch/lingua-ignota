module Language.Number exposing
  ( Number (..)
  , translate
  )

import Language.Decode as Decode


type Number
  = Number Int


decoder : Decode.Class Number
decoder =
  Decode.Class
    { classIndex =
      12
    , decoder =
      ( \ (Number n) -> n )
    }


translate : { a | baseSystem : Int, reverseDictionary : Int -> List b } -> Number -> List b
translate lang (Number n) =
  if
    n > lang.baseSystem
  then
    modBy lang.baseSystem n
    |> Number
    |> Decode.translate decoder lang
    |> (++) (translate lang (Number (n // lang.baseSystem)))
  else
    n
    |> Number
    |> Decode.translate decoder lang
