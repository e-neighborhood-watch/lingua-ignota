module Language.Pronoun.Personal exposing
  ( PersonalPronoun (..)
  , translate
  , normalize
  , decoder
  )

import Language exposing
  ( Language
  )
import Language.Decode as Decode
import Language.GrammaticalNumber as GrammaticalNumber exposing
  ( GrammaticalNumber
  )
import Language.Phoneme exposing
  ( Phoneme
  )

-- Here we use "she" to mean any person.
-- No gender has been implemented.
-- We do not use the normal singular "they"
-- because it is important to distinguish between singular and plural.
-- We also use "you" exclusively in the singular sense
type PersonalPronoun
  = Me
  | MeYou -- Second person dual
  | MeHer -- Second person dual (default when language does not have clusivity)
  | MeThem
  | MeYouThem
  | You
  | YouHer -- Second person dual
  | YouThem
  | Her
  | HerHer -- Third person dual
  | Them

decoder : Decode.Class PersonalPronoun
decoder =
  Decode.Class
    { classIndex =
      4
    , decoder =
      ( \ pronoun ->
        case
          pronoun
        of
          Me ->
            0
          MeYou ->
            1
          MeHer ->
            2
          MeThem ->
            3
          MeYouThem ->
            4
          You ->
            5
          YouHer ->
            6
          YouThem ->
            7
          Her ->
            8
          HerHer ->
            9
          Them ->
            10
      )
    }

translate : Language -> PersonalPronoun -> List Phoneme
translate language pp =
  Decode.translate
    decoder
    language
    ( normalize
      language
      pp
    )

normalize : Language -> PersonalPronoun -> PersonalPronoun
normalize { clusivity, grammaticalNumber } pronoun =
  let
    cNormalized : PersonalPronoun
    cNormalized =
      if
        clusivity
      then
        pronoun
      else
        case
          pronoun
        of
          MeYou ->
            MeHer
          MeYouThem ->
            MeThem
          x ->
            x

    cAndNNormalized : PersonalPronoun
    cAndNNormalized =
      case
        grammaticalNumber
      of
        GrammaticalNumber.None ->
          case
            pronoun
          of
            MeThem ->
              Me
            MeYouThem ->
              Me
            YouHer ->
              You
            YouThem ->
              You
            HerHer ->
              Her
            Them ->
              Her
            x ->
              x
        GrammaticalNumber.SingularPlural False _ ->
          case
            pronoun
          of
            MeYou ->
              MeYouThem
            MeHer ->
              MeThem
            YouHer ->
              YouThem
            HerHer ->
              Them
            x ->
              x
        GrammaticalNumber.SingularPlural True _ ->
          pronoun
  in
    cAndNNormalized
