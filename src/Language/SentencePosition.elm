module Language.SentencePosition exposing
  ( SentencePosition (..)
  )

type SentencePosition
  = Sentence
  | Verb
  | Subject
