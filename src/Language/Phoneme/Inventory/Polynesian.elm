module Language.Phoneme.Inventory.Polynesian
  exposing
    ( generate
    , Inventory
    )


import Extra.Random as Random
import Random
import Language.Phoneme as Phoneme
  exposing
    ( Phoneme
    )


type alias Inventory =
  { nasals :
    List (List Phoneme)
  , plosives :
    List (List Phoneme)
  , liquids :
    List (List Phoneme)
  , miscConsonants :
    List (List Phoneme)
  , monophthongs :
    List (List Phoneme)
  , dipthongs :
    List (List Phoneme)
  }


generateNasals : Random.Generator (List (List Phoneme))
generateNasals =
  Random.uniform
    -- Tahitian
    ( List.map List.singleton
      [ Phoneme.xsampa "m"
      , Phoneme.xsampa "n"
      ]
    )
    -- Hawai'ian
    [ ( List.map List.singleton
        [ Phoneme.xsampa "m"
        , Phoneme.xsampa "n"
        ]
      )
    -- Samoan
    , ( List.map List.singleton
        [ Phoneme.xsampa "m"
        , Phoneme.xsampa "n"
        , Phoneme.xsampa "N"
        ]
      )
    -- Tongan
    , ( List.map List.singleton
        [ Phoneme.xsampa "m"
        , Phoneme.xsampa "n"
        , Phoneme.xsampa "N"
        ]
      )
    -- Maori
    , ( List.map List.singleton
        [ Phoneme.xsampa "m"
        , Phoneme.xsampa "n"
        , Phoneme.xsampa "N"
        ]
      )
    -- Pukapukan
    , ( List.map List.singleton
        [ Phoneme.xsampa "m"
        , Phoneme.xsampa "n"
        , Phoneme.xsampa "N"
        ]
      )
    -- Rennellese
    , ( List.map List.singleton
        [ Phoneme.xsampa "m"
        , Phoneme.xsampa "n"
        , Phoneme.xsampa "N"
        ]
      )
    -- Futuna
    , ( List.map List.singleton
        [ Phoneme.xsampa "m"
        , Phoneme.xsampa "n"
        , Phoneme.xsampa "N"
        ]
      )
    -- Mele
    , ( List.map List.singleton
        [ Phoneme.xsampa "m"
        , Phoneme.xsampa "m_w"
        , Phoneme.xsampa "n"
        , Phoneme.xsampa "N"
        ]
      )
    -- Emae
    , ( List.map List.singleton
        [ Phoneme.xsampa "m_w"
        , Phoneme.xsampa "n"
        , Phoneme.xsampa "N"
        ]
      )
    -- Nukuria
    , ( List.map List.singleton
        [ Phoneme.xsampa "m"
        , Phoneme.xsampa "n"
        , Phoneme.xsampa "N"
        ]
      )
    -- Marquesan
    , [ [ Phoneme.xsampa "m"
        ]
      , [ Phoneme.xsampa "n"
        ]
      , [ Phoneme.xsampa "N"
        , Phoneme.xsampa "k"
        ]
      ]
    ]


generatePlosives : Random.Generator (List (List Phoneme))
generatePlosives =
  Random.map (List.map List.singleton)
  ( Random.uniform
    [ Phoneme.xsampa "p"
    , Phoneme.xsampa "t"
    , Phoneme.xsampa "?"
    ]
    -- Hawai'ian
    [ [ Phoneme.xsampa "p"
      , Phoneme.xsampa "k"
      , Phoneme.xsampa "?"
      ]
    -- Samoan
    , [ Phoneme.xsampa "p"
      , Phoneme.xsampa "t"
      , Phoneme.xsampa "?"
      ]
    -- Tongan
    , [ Phoneme.xsampa "p"
      , Phoneme.xsampa "t"
      , Phoneme.xsampa "k"
      , Phoneme.xsampa "?"
      ]
    -- Maori
    , [ Phoneme.xsampa "p"
      , Phoneme.xsampa "t"
      , Phoneme.xsampa "k"
      ]
    -- Pukapukan
    , [ Phoneme.xsampa "p"
      , Phoneme.xsampa "t"
      , Phoneme.xsampa "k"
      ]
    -- Rennellese
    , [ Phoneme.xsampa "p"
      , Phoneme.xsampa "t"
      , Phoneme.xsampa "k"
      , Phoneme.xsampa "g" -- Prenasalized
      , Phoneme.xsampa "?"
      ]
    -- Futuna
    , [ Phoneme.xsampa "p"
      , Phoneme.xsampa "t"
      , Phoneme.xsampa "k"
      ]
    -- Mele
    , [ Phoneme.xsampa "p"
      , Phoneme.xsampa "p_w"
      , Phoneme.xsampa "t"
      , Phoneme.xsampa "k"
      ]
    -- Emae
    , [ Phoneme.xsampa "p"
      , Phoneme.xsampa "b" -- Prenasalized
      , Phoneme.xsampa "t"
      , Phoneme.xsampa "d" -- Prenasalized
      , Phoneme.xsampa "k"
      ]
    -- Nukuria
    , [ Phoneme.xsampa "p"
      , Phoneme.xsampa "t"
      , Phoneme.xsampa "k"
      , Phoneme.xsampa "b"
      , Phoneme.xsampa "g"
      ]
    -- Marquesan
    , [ Phoneme.xsampa "p"
      , Phoneme.xsampa "t"
      , Phoneme.xsampa "k"
      , Phoneme.xsampa "?"
      ]
    ]
  )

generateMiscConsonants : Random.Generator (List (List Phoneme))
generateMiscConsonants =
  Random.uniform
    -- Tahitian
    [ Phoneme.xsampa "f"
    , Phoneme.xsampa "v"
    , Phoneme.xsampa "h"
    ]
    -- Hawai'ian
    [ [ Phoneme.xsampa "h"
      , Phoneme.xsampa "w"
      ]
    -- Samoan
    , [ Phoneme.xsampa "f"
      , Phoneme.xsampa "v"
      , Phoneme.xsampa "s"
      ]
    -- Tongan
    , [ Phoneme.xsampa "f"
      , Phoneme.xsampa "v"
      , Phoneme.xsampa "s"
      , Phoneme.xsampa "h"
      ]
    -- Maori
    , [ Phoneme.xsampa "p\\"
      , Phoneme.xsampa "h"
      , Phoneme.xsampa "4"
      , Phoneme.xsampa "w"
      ]
    -- Pukapukan
    , [ Phoneme.xsampa "v"
      , Phoneme.xsampa "T" -- Should be palatalized
      , Phoneme.xsampa "w"
      ]
    -- Rennellese
    , [ Phoneme.xsampa "B"
      , Phoneme.xsampa "s"
      , Phoneme.xsampa "G"
      , Phoneme.xsampa "h"
      ]
    -- Futuna
    , [ Phoneme.xsampa "f"
      , Phoneme.xsampa "B"
      , Phoneme.xsampa "s"
      , Phoneme.xsampa "S_w"
      , Phoneme.xsampa "h"
      ]
    -- Mele
    , [ Phoneme.xsampa "t_s\\"
      , Phoneme.xsampa "f"
      , Phoneme.xsampa "s"
      , Phoneme.xsampa "v"
      , Phoneme.xsampa "h"
      ]
    -- Emae
    , [ Phoneme.xsampa "f"
      , Phoneme.xsampa "v"
      , Phoneme.xsampa "s"
      ]
    -- Nukuria
    , [ Phoneme.xsampa "f"
      , Phoneme.xsampa "s"
      , Phoneme.xsampa "h"
      , Phoneme.xsampa "v"
      , Phoneme.xsampa "w"
      ]
    -- Marquesan
    , [  Phoneme.xsampa "f"
      ,  Phoneme.xsampa "v"
      ,  Phoneme.xsampa "h"
      ]
    ]
  |> Random.map (List.map List.singleton)


generateLiquids : Random.Generator (List (List Phoneme))
generateLiquids =
  Random.uniform
    -- Tahitian
    [ Phoneme.xsampa "r"
    ]
    -- Hawai'ian
    [ [ Phoneme.xsampa "4"
      ]
    -- Samoan
    , [ Phoneme.xsampa "l"
      ]
    -- Tongan
    , [ Phoneme.xsampa "l"
      ]
    -- Maori
    , [ Phoneme.xsampa "4"
      ]
    -- Pukapukan
    , [ Phoneme.xsampa "l"
      ]
    -- Rennellese
    , [ Phoneme.xsampa "l"
      ]
    -- Futuna
    , [ Phoneme.xsampa "r"
      , Phoneme.xsampa "l"
      ]
    -- Mele
    , [ Phoneme.xsampa "r"
      , Phoneme.xsampa "l"
      ]
    -- Emae
    , [ Phoneme.xsampa "r"
      , Phoneme.xsampa "l"
      ]
    -- Nukuria
    , [ Phoneme.xsampa "l"
      , Phoneme.xsampa "r"
      ]
    -- Marquesan
    , [ Phoneme.xsampa "r"
      ]
    ]
  |> Random.map (List.map List.singleton)


generateMonophthongs : Random.Generator (List (List Phoneme))
generateMonophthongs =
  Random.uniform
    [ Phoneme.xsampa "a"
    , Phoneme.xsampa "e"
    , Phoneme.xsampa "i"
    , Phoneme.xsampa "o"
    , Phoneme.xsampa "u"
    ]
    [ [ Phoneme.xsampa "a"
      , Phoneme.xsampa "e"
      , Phoneme.xsampa "i"
      , Phoneme.xsampa "o"
      , Phoneme.xsampa "u"
      , Phoneme.xsampa "a:"
      , Phoneme.xsampa "e:"
      , Phoneme.xsampa "i:"
      , Phoneme.xsampa "o:"
      , Phoneme.xsampa "u:"
      ]
    ]
  |> Random.map (List.map List.singleton)


generateDiphthongs : Random.Generator (List (List Phoneme))
generateDiphthongs =
  Random.uniform
  []
  [ [ [ Phoneme.xsampa "o"
      , Phoneme.xsampa "a"
      ]
    , [ Phoneme.xsampa "u"
      , Phoneme.xsampa "a"
      ]
    , [ Phoneme.xsampa "a"
      , Phoneme.xsampa "i"
      ]
    ]
  ]


generate : Random.Generator Inventory
generate =
  Random.map
    Inventory
    generateNasals
  |> Random.apTo generatePlosives
  |> Random.apTo generateLiquids
  |> Random.apTo generateMiscConsonants
  |> Random.apTo generateMonophthongs
  |> Random.apTo generateDiphthongs
