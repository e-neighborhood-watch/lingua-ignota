module Language.MorphosyntacticAlignment exposing
  ( MorphosyntacticAlignment (..)
  , generate
  )

import Random

type MorphosyntacticAlignment
  = NominativeAccusative
  | ErgativeAbsolutive

generate : Random.Generator MorphosyntacticAlignment
generate =
  Random.uniform
    NominativeAccusative
    [ ErgativeAbsolutive
    ]
