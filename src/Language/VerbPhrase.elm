module Language.VerbPhrase exposing
  ( VerbPhrase (..)
  )

import Language.Basic.Verb.Transitive exposing
  ( TransitiveVerb
  )
import Language.NounPhrasal exposing
  ( NounPhrasal
  )
import Language.SubjectComplement exposing
  ( SubjectComplement
  )
import Language.Verb.Ditransitive exposing
  ( DitransitiveVerb
  )
import Language.Verb.Intransitive exposing
  ( IntransitiveVerb
  )

type VerbPhrase
  = Ditransitive DitransitiveVerb
    { subject :
      NounPhrasal
    , directObject :
      NounPhrasal
    , indirectObject :
      NounPhrasal
    }
  | Transitive TransitiveVerb
    { subject :
      NounPhrasal
    , directObject :
      NounPhrasal
    }
  | Intransitive IntransitiveVerb
    { subject :
      NounPhrasal
    }
  | Copula
    { subject :
      NounPhrasal
    , complement :
      SubjectComplement
    }
