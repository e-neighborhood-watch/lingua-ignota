module Language.Conjunction.Noun exposing
  ( Conjunction (..)
  , translate
  )

import Language exposing
  ( Language
  )
import Language.Phoneme exposing
  ( Phoneme
  )
import Language.Conjunction.Sentence as Sentence

type Conjunction
  = Cumulative
  | Alternative

translate : Language -> Conjunction -> List Phoneme
translate lang conjunction =
  case
    conjunction
  of
    Cumulative ->
      Sentence.translate lang Sentence.Cumulative
    Alternative ->
      Sentence.translate lang Sentence.Alternative
