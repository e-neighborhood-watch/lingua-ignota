module Language.Verb.Intransitive exposing
  ( IntransitiveVerb (..)
  , translate
  )

import Language exposing
  ( Language
  )
import Language.Decode as Decode
import Language.Phoneme exposing
  ( Phoneme
  )

type IntransitiveVerb
  = Die
  | Sleep

decoder : Decode.Class IntransitiveVerb
decoder =
  Decode.Class
    { classIndex =
      3
    , decoder =
      ( \ word ->
        case
          word
        of
          Die ->
            0
          Sleep ->
            1
      )
    }

translate : Language -> IntransitiveVerb -> List Phoneme
translate =
  Decode.translate decoder
