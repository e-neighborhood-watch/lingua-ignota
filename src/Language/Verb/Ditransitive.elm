module Language.Verb.Ditransitive exposing
  ( DitransitiveVerb (..)
  , translate
  )

import Language exposing
  ( Language
  )
import Language.Decode as Decode
import Language.Phoneme exposing
  ( Phoneme
  )

type DitransitiveVerb
  = Give
  | AskFor
  | Get

translate : Language -> DitransitiveVerb -> List Phoneme
translate =
  Decode.translate decoder

decoder : Decode.Class DitransitiveVerb
decoder =
  Decode.Class
    { classIndex =
      1
    , decoder =
      ( \ verb ->
        case
          verb
        of
          Give ->
            0
          AskFor ->
            1
          Get ->
            2
      )
    }

