module Language.Verb.Transitive
  exposing
    ( translate
    )


import Language
  exposing
    ( Language
    )
import Language.Basic.Verb.Transitive exposing (..)
import Language.Decode as Decode
import Language.Etymology.Verb.Transitive as Etymology
  exposing
    ( Etymology
    )
import Language.Phoneme
  exposing
    ( Phoneme
    )

import Random


type alias Ontology =
  TransitiveVerb -> List Etymology


ontology : Ontology
ontology verb =
  case
    verb
  of
    Kill ->
      [
      ]
    Eat ->
      [
      ]
    Retrive ->
      [
      ]
    Use ->
      [
      ]
    Want ->
      [
      ]
    Find ->
      [
      ]
    Need ->
      [
      ]
    Cook ->
      [
      ]
    Bake ->
      [
      ]
    Brew ->
      [
      ]
    Pick ->
      [
      ]
    PickUp ->
      [
      ]
    GoTo ->
      [
      ]


translate : Language -> TransitiveVerb -> List Phoneme
translate lang verb =
  lang.etymologySeed
  + Decode.decode decoder verb
  |> Random.initialSeed
  |>
    (( verb
    |> ontology
    |> Random.uniform (Etymology.Inherit verb)
    |> Random.step
    ))
  |> Tuple.first
  |> Etymology.translate lang
