module Language.Sentence
  exposing
    ( Sentence (..)
    , translate
    , tests
    , isQuestion
    )


import Language
  exposing
    ( Language
    )
import Language.Adjective as Adjective
  exposing
    ( Adjective
    )
import Language.Adverb as Adverb
  exposing
    ( Adverb
    )
import Language.Basic.Noun as Noun
import Language.Basic.Verb.Transitive as TransitiveVerb
  exposing
    ( TransitiveVerb
    )
import Language.Conjunction.Sentence as SentenceConjunction
import Language.Conjunction.Noun as NounConjunction
import Language.Copula as Copula
import Language.Decode as Decode
import Language.Determiner as Determiner
import Language.Fixing as Fixing
  exposing
    ( Fixing
    )
import Language.Marking.Verb as VerbMarking
  exposing
    ( VerbMarking (..)
    )
import Language.Mood as Mood
  exposing
    ( Mood
    )
import Language.MorphosyntacticAlignment as MorphosyntacticAlignment
import Language.NounPhrasal as NounPhrasal
  exposing
    ( NounPhrasal
    )
import Language.Number as Number
import Language.Particle.Misc as MiscParticle
import Language.Particle.Number as NumberParticle
import Language.Phoneme as Phoneme
  exposing
    ( Phoneme
    )
import Language.Pronoun.Personal as PersonalPronoun
import Language.SentencePosition as SentencePosition
  exposing
    ( SentencePosition
    )
import Language.SentenceStructure as SentenceStructure
import Language.SentenceStructure.Copula as CopulaStructure
import Language.SubjectComplement as SubjectComplement
  exposing
    ( SubjectComplement
    )
import Language.Verb.Ditransitive as DitransitiveVerb
  exposing
    ( DitransitiveVerb
    )
import Language.Verb.Transitive as TransitiveVerb
import Language.Verb.Intransitive as IntransitiveVerb
  exposing
    ( IntransitiveVerb
    )
import Language.VerbPhrase as VerbPhrase
  exposing
    ( VerbPhrase
    )


type Sentence
  = Sentence Mood (Maybe Adverb) VerbPhrase
  | Conjunction SentenceConjunction.Conjunction Sentence Sentence
  | Conditional (Maybe Adverb) VerbPhrase (Maybe Adverb) VerbPhrase


applyParticle : VerbMarking -> phonemes -> SentencePosition -> List phonemes -> List phonemes
applyParticle verbMarking particle sentencePos =
  case
    verbMarking
  of
    VerbMarking targetPos fixing ->
      if
        targetPos == sentencePos
      then
        particle
        |> Fixing.apply fixing
      else
        identity


applyMoodParticle : Language -> Mood -> SentencePosition -> List (List Phoneme) -> List (List Phoneme)
applyMoodParticle lang mood =
  case
    mood
  of
    Mood.Indicative ->
      always identity
    _ ->
      mood
      |> Mood.translate lang
      |> applyParticle lang.imperativeMarking


applyAdverb : Language -> Maybe Adverb -> SentencePosition -> List (List Phoneme) -> List (List Phoneme)
applyAdverb lang adverb =
  case
    adverb
  of
    Just trueAdverb ->
      trueAdverb
      |> Adverb.translate lang
      |> applyParticle lang.adverbMarking
    Nothing ->
      always identity


translate : Language -> Sentence -> List (List Phoneme)
translate lang sentence =
  case
    sentence
  of
    Sentence mood adverb verbPhrase ->
      ( case
          verbPhrase
        of
          VerbPhrase.Ditransitive verb { subject, directObject, indirectObject } ->
            let
              verbPhonemes : List (List Phoneme)
              verbPhonemes =
                verb
                |> DitransitiveVerb.translate lang
                |> List.singleton
                |> applyMoodParticle lang mood SentencePosition.Verb
                |> applyAdverb lang adverb SentencePosition.Verb
            in
              SentenceStructure.createOrder
                lang.sentenceStructure
                { verb =
                  verbPhonemes
                , subject =
                  (NounPhrasal.translate lang subject)
                , directObject =
                  (NounPhrasal.translate lang directObject)
                , indirectObject =
                  (NounPhrasal.translate lang indirectObject)
                }

          VerbPhrase.Transitive verb { subject, directObject } ->
            let
              verbPhonemes : List (List Phoneme)
              verbPhonemes =
                verb
                |> TransitiveVerb.translate lang
                |> List.singleton
                |> applyMoodParticle lang mood SentencePosition.Verb
                |> applyAdverb lang adverb SentencePosition.Verb
            in
              SentenceStructure.createOrder
                lang.sentenceStructure
                { verb =
                  verbPhonemes
                , subject =
                  (NounPhrasal.translate lang subject)
                , directObject =
                  (NounPhrasal.translate lang directObject)
                , indirectObject =
                  []
                }

          VerbPhrase.Intransitive verb { subject } ->
            let
              verbPhonemes : List (List Phoneme)
              verbPhonemes =
                verb
                |> IntransitiveVerb.translate lang
                |> List.singleton
                |> applyMoodParticle lang mood SentencePosition.Verb
                |> applyAdverb lang adverb SentencePosition.Verb
            in
              case
                lang.morphosyntacticAlignment
              of
                MorphosyntacticAlignment.NominativeAccusative ->
                  SentenceStructure.createOrder
                    lang.sentenceStructure
                    { verb =
                      verbPhonemes
                    , subject =
                      (NounPhrasal.translate lang subject)
                    , directObject =
                      []
                    , indirectObject =
                      []
                    }
                  |> applyMoodParticle lang mood SentencePosition.Sentence
                MorphosyntacticAlignment.ErgativeAbsolutive ->
                  SentenceStructure.createOrder
                    lang.sentenceStructure
                    { verb =
                      verbPhonemes
                    , subject =
                      []
                    , directObject =
                      (NounPhrasal.translate lang subject)
                    , indirectObject =
                      []
                    }

          VerbPhrase.Copula { subject, complement } ->
            let
              copulaPhonemes : List (List Phoneme)
              copulaPhonemes =
                ()
                |> Copula.translate lang
                |> List.singleton
                |> applyMoodParticle lang mood SentencePosition.Verb
                |> applyAdverb lang adverb SentencePosition.Verb
            in
              CopulaStructure.createOrder
                lang.copulaStructure
                copulaPhonemes
                ( NounPhrasal.translate lang subject )
                ( SubjectComplement.translate lang complement )
      )
      |> applyMoodParticle lang mood SentencePosition.Sentence
      |> applyAdverb lang adverb SentencePosition.Sentence

    Conjunction conjunct sentence1 sentence2 ->
      List.concat
        [ translate lang sentence1
        , conjunct
          |> SentenceConjunction.translate lang
          |> List.singleton
        , translate lang sentence2
        ]

    Conditional adverb1 verbPhrase1 adverb2 verbPhrase2 ->
      List.concat
       [ translate lang
         ( Sentence Mood.Indicative
           adverb1
           verbPhrase1
         )
       , MiscParticle.translate lang MiscParticle.Conditional
         |> List.singleton
       , translate lang
         ( Sentence Mood.Conditional
           adverb2
           verbPhrase2
         )
       ]


isQuestion : Sentence -> Bool
isQuestion sentence =
  case
    sentence
  of
    Sentence Mood.Interogative _ _ ->
      True
    _ ->
      False


tests : List Sentence
tests =
  [ Conjunction
    SentenceConjunction.Illative
    ( Sentence
      Mood.Indicative
      Nothing
      ( VerbPhrase.Transitive
        TransitiveVerb.Need
        { subject =
          ( NounPhrasal.Pronoun
            PersonalPronoun.Me
          )
        , directObject =
          ( NounPhrasal.NumberPhrase
            ( Number.Number 19 )
            ( NounPhrasal.Noun
              Noun.Mushroom
              []
              ( Determiner.Article
                False
                NumberParticle.Plural
              )
              Nothing
            )
          )
        }
      )
    )
    ( Sentence
      Mood.Indicative
      ( Just
        Adverb.Tonight
      )
      ( VerbPhrase.Transitive
        TransitiveVerb.Cook
        { subject =
          ( NounPhrasal.Pronoun
            PersonalPronoun.Me
          )
        , directObject =
          ( NounPhrasal.Noun
            Noun.Stew
            []
            ( Determiner.Article
              False
              NumberParticle.Plural
            )
            Nothing
          )
        }
      )
    )
  , Conjunction
    SentenceConjunction.Cumulative
    ( Sentence
      Mood.Imperative
      Nothing
      ( VerbPhrase.Transitive
        TransitiveVerb.GoTo
        { subject =
          ( NounPhrasal.Pronoun
            PersonalPronoun.You
          )
        , directObject =
          ( NounPhrasal.Noun
            Noun.Forrest
            []
            ( Determiner.Article
              True
              NumberParticle.Singular
            )
            Nothing
          )
        }
      )
    )
    ( Sentence
      Mood.Imperative
      Nothing
      ( VerbPhrase.Transitive
        TransitiveVerb.Pick
        { subject =
          ( NounPhrasal.Pronoun
            PersonalPronoun.You
          )
        , directObject =
          ( NounPhrasal.Pronoun
            PersonalPronoun.Them
          )
        }
      )
    )
  , Conjunction
    SentenceConjunction.Illative
    ( Sentence
      Mood.Prohibitive
      Nothing
      ( VerbPhrase.Transitive
        TransitiveVerb.Pick
        { subject =
          ( NounPhrasal.Pronoun
            PersonalPronoun.You
          )
        , directObject =
          ( NounPhrasal.Noun
            Noun.Mushroom
            [ Adjective.Small
            ]
            ( Determiner.Article
              True
              NumberParticle.Plural
            )
            Nothing
          )
        }
      )
    )
    ( Sentence
      Mood.Indicative
      Nothing
      ( VerbPhrase.Copula
        { subject =
          ( NounPhrasal.Pronoun
            PersonalPronoun.Them
          )
        , complement =
          ( SubjectComplement.AdjectivePhrase
            Adjective.Poisonous
          )
        }
      )
    )
  ]

