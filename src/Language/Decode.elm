module Language.Decode exposing
  ( Class (..)
  , translate
  , totalClasses
  , decode
  )

totalClasses : Int
totalClasses =
  14

type Class a
  = Class
    { classIndex :
      Int
    , decoder :
      a -> Int
    }

decode : Class a -> a -> Int
decode (Class { classIndex, decoder }) thing =
  decoder thing * totalClasses + classIndex

translate : Class a -> { c | reverseDictionary : Int -> List b } -> a -> List b
translate decoder { reverseDictionary } word =
  word
  |> decode decoder
  |> reverseDictionary
