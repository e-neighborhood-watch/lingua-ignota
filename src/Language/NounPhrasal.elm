module Language.NounPhrasal exposing
  ( NounPhrasal (..)
  , translate
  )

import Language
  exposing
    ( Language
    )
import Language.Adjective as Adjective
  exposing
    ( Adjective
    )
import Language.Basic.Noun as Noun
  exposing
    ( Noun
    )
import Language.Conjunction.Noun as Conjunction
  exposing
    ( Conjunction
    )
import Language.Decode as Decode
import Language.Determiner as Determiner
  exposing
    ( Determiner
    )
import Language.Fixing as Fixing
import Language.Noun as Noun
import Language.Number as Number
  exposing
    ( Number
    )
import Language.Particle.Misc as MiscParticle
import Language.Phoneme
  exposing
    ( Phoneme
    )
import Language.Pronoun.Personal as PersonalPronoun
  exposing
    ( PersonalPronoun
    )
import Language.Particle.Number as NumberParticle
  exposing
    ( NumberParticle
    )
import Language.GrammaticalNumber as GrammaticalNumber
  exposing
    ( GrammaticalNumber
    )


type NounPhrasal
  = Noun Noun (List Adjective) Determiner (Maybe NounPhrasal)
  | Pronoun PersonalPronoun
  | NumberPhrase Number NounPhrasal
  | Conjunction Conjunction NounPhrasal NounPhrasal

translate : Language -> NounPhrasal -> List (List Phoneme)
translate lang nounPhrasal =
  case
    nounPhrasal
  of
    Conjunction conjunction phrase1 phrase2 ->
      List.concat
        [ translate lang phrase1
        , Conjunction.translate lang conjunction
          |> List.singleton
        , translate lang phrase2
        ]
    NumberPhrase number phrase ->
      case
        lang.compositionalPhraseFixing
      of
        Fixing.Pre ->
          translate lang phrase
          ++ [ MiscParticle.translate lang MiscParticle.Composition ]
          ++ [ Number.translate lang number ]
        Fixing.Post ->
          [ Number.translate lang number ]
          ++ [ MiscParticle.translate lang MiscParticle.Composition ]
          ++ translate lang phrase
    Noun noun adjectives determiner compositional ->
      let
        translatedAdjectives : List (List Phoneme)
        translatedAdjectives =
          List.map
            (Adjective.translate lang)
            adjectives

        translatedParticle : List (List Phoneme)
        translatedParticle =
          Determiner.translate lang determiner

        simpleNoun : List (List Phoneme)
        simpleNoun =
          Fixing.apply
            lang.determinerFixing
            (Noun.translate lang noun)
            translatedParticle

        complexNoun : List (List Phoneme)
        complexNoun =
          case
            lang.adjectiveFixing
          of
            Fixing.Pre ->
              translatedAdjectives ++ simpleNoun
            Fixing.Post ->
              simpleNoun ++ translatedAdjectives
      in
        case
          compositional
        of
          Nothing ->
            complexNoun
          Just phrase ->
            let
              compositionalPhrase : List (List Phoneme)
              compositionalPhrase =
                translate lang phrase
            in
              case
                lang.compositionalPhraseFixing
              of
                Fixing.Pre ->
                  translate lang phrase
                  ++ [ MiscParticle.translate lang MiscParticle.Composition ]
                  ++ complexNoun
                Fixing.Post ->
                  complexNoun
                  ++ [ MiscParticle.translate lang MiscParticle.Composition ]
                  ++ translate lang phrase


    Pronoun pronoun ->
      pronoun
      |> PersonalPronoun.translate lang
      |> List.singleton
