module Language.Copula exposing
  ( translate
  )

import Language.Decode as Decode
import Language exposing
  ( Language
  )
import Language.Phoneme exposing
  ( Phoneme
  )

decoder : Decode.Class ()
decoder =
  Decode.Class
    { classIndex =
      8
    , decoder =
      always 0
    }

translate : Language -> () -> List Phoneme
translate = Decode.translate decoder
