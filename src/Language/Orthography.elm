module Language.Orthography
  exposing
    ( Orthography
    , ascii
    , ipa
    )


import Language.Phoneme as Phoneme
  exposing
    ( Phoneme (..)
    , Voicing (..)
    , ArticulationPoint (..)
    , Closeness (..)
    , Backness (..)
    , Rounding (..)
    , Length (..)
    , RawConsonant (..)
    , Aspiration (..)
    , Labialization (..)
    , Lateralization (..)
    )


type alias Orthography =
  Phoneme -> String


offset : ArticulationPoint -> Voicing -> Char -> Char
offset articulationPoint voicing =
  let
    voiceOffset =
      case
        voicing
      of
        Voiced ->
          1
        Voiceless ->
          0

    articulationOffset =
      case
        articulationPoint
      of
        Glottal ->
          10
        Epiglottal ->
          9
        Uvular ->
          8
        Velar ->
          7
        Palatal ->
          6
        -- Reserved for Retroflex
        PostAlveolar ->
          4
        Alveolar ->
          3
        Dental ->
          2
        Labiodental ->
          1
        Bilabial ->
          0
  in
    Char.toCode
    >> (+) (2 * articulationOffset)
    >> (+) voiceOffset
    >> Char.fromCode


vowelOffset : Rounding -> Backness -> Char -> Char
vowelOffset rounding backness =
  let
    roundingOffset =
      case
        rounding
      of
        Rounded ->
          1
        Unrounded ->
          0

    backnessOffset =
      case
        backness
      of
        Front ->
          0
        Central ->
          1
        Back ->
          2
  in
    Char.toCode
    >> (+) (2 * backnessOffset)
    >> (+) roundingOffset
    >> Char.fromCode


ipa phoneme =
  case
    phoneme
  of
    Vowel rounding backness closeness Long ->
      ipa (Vowel rounding backness closeness Short)
      ++ "ĕ"
    Vowel rounding backness Close Short ->
      vowelOffset rounding backness 'Ƕ'
      |> String.fromChar
    Vowel rounding backness CloseMid Short ->
      vowelOffset rounding backness 'ǖ'
      |> String.fromChar
    Vowel rounding backness OpenMid Short ->
      vowelOffset rounding backness 'ƶ'
      |> String.fromChar
    Vowel rounding backness Open Short ->
      vowelOffset rounding backness 'Ɩ'
      |> String.fromChar
    Consonant voicing Labialized (Approximant Centralized Velar) ->
      offset Velar voicing 'ƀ'
      |> String.fromChar
    Consonant voicing Labialized consonant ->
      ipa (Consonant voicing Unlabialized consonant)
      ++ "ƴ"
    Consonant voicing Unlabialized (Plosive Aspirated location) ->
      ipa (Consonant voicing Unlabialized (Plosive Unaspirated location))
      ++ "ǳ"
    Consonant voicing Unlabialized (Plosive Unaspirated articulationPoint) ->
      offset articulationPoint voicing 'Ǡ'
      |> String.fromChar
    Consonant voicing Unlabialized (Nasal articulationPoint) ->
      offset articulationPoint voicing 'ǀ'
      |> String.fromChar
    Consonant voicing Unlabialized (Trill articulationPoint) ->
      offset articulationPoint voicing 'Ơ'
      |> String.fromChar
    Consonant voicing Unlabialized (Tap articulationPoint) ->
      offset articulationPoint voicing 'ƀ'
      |> String.fromChar
    Consonant voicing Unlabialized (Fricative Centralized articulationPoint) ->
      offset articulationPoint voicing 'Š'
      |> String.fromChar
    Consonant voicing Unlabialized (Fricative Lateralized articulationPoint) ->
      offset articulationPoint voicing 'ŀ'
      |> String.fromChar
    Consonant voicing Unlabialized (Approximant Centralized articulationPoint) ->
      offset articulationPoint voicing 'Ġ'
      |> String.fromChar
    Consonant voicing Unlabialized (Approximant Lateralized articulationPoint) ->
      offset articulationPoint voicing 'Ā'
      |> String.fromChar
    _ ->
      "?"

-- ĀāĂăĄąĆćĈĉĊċČčĎďĐđĒēĔĕĖėĘęĚěĜĝĞğ
-- ĠġĢģĤĥĦħĨĩĪīĬĭĮįİıĲĳĴĵĶķĸĹĺĻļĽľĿ
-- ŀŁłŃńŅņŇňŉŊŋŌōŎŏŐőŒœŔŕŖŗŘřŚśŜŝŞş
-- ŠšŢţŤťŦŧŨũŪūŬŭŮůŰűŲųŴŵŶŷŸŹźŻżŽžſ
-- ƀƁƂƃƄƅƆƇƈƉƊƋƌƍƎƏƐƑƒƓƔƕƖƗƘƙƚƛƜƝƞƟ
-- ƠơƢƣƤƥƦƧƨƩƪƫƬƭƮƯưƱƲƳƴƵƶƷƸƹƺƻƼƽƾƿ
-- ǀǁǂǃǄǅǆǇǈǉǊǋǌǍǎǏǐǑǒǓǔǕǖǗǘǙǚǛǜǝǞǟ
-- ǠǡǢǣǤǥǦǧǨǩǪǫǬǭǮǯǰǱǲǳǴǵǶǷǸǹǺǻǼǽǾǿ

ascii : Orthography
ascii phoneme =
  case
    phoneme
  of
    Consonant Voiceless Unlabialized (Plosive Unaspirated Bilabial) ->
      "p"
    Consonant Voiceless Labialized (Plosive Unaspirated Bilabial) ->
      "pw"
    Consonant Voiceless Unlabialized (Plosive Unaspirated Velar) ->
      "k"
    Consonant Voiceless Unlabialized (Plosive Unaspirated Alveolar) ->
      "t"
    Consonant Voiced _ (Plosive Unaspirated Bilabial) ->
      "b"
    Consonant Voiced Unlabialized (Plosive Unaspirated Velar) ->
      "g"
    Consonant Voiced Unlabialized (Plosive Unaspirated Alveolar) ->
      "d"
    Consonant _ Unlabialized (Plosive Unaspirated Glottal) ->
      "'"
    Consonant _ Unlabialized (Fricative Centralized Glottal) ->
      "h"
    Consonant Voiceless Unlabialized (Fricative Centralized Alveolar) ->
      "s"
    Consonant Voiced Unlabialized (Fricative Centralized Alveolar) ->
      "z"
    Consonant Voiceless _ (Fricative Centralized PostAlveolar) ->
      "sh"
    Consonant Voiced _ (Fricative Centralized PostAlveolar) ->
      "zh"
    Consonant Voiceless Unlabialized (Fricative Centralized Labiodental) ->
      "f"
    Consonant Voiced Unlabialized (Fricative Centralized Labiodental) ->
      "v"
    Consonant Voiced Unlabialized (Tap Alveolar) ->
      "r"
    Consonant Voiced Unlabialized (Trill Alveolar) ->
      "rr"
    Consonant Voiced Unlabialized (Approximant Lateralized Alveolar) ->
      "l"
    Consonant Voiced Labialized (Approximant Centralized Velar) ->
      "w"
    Consonant Voiced Unlabialized (Approximant Centralized Palatal) ->
      "y"
    Consonant Voiced Unlabialized (Nasal Bilabial) ->
      "m"
    Consonant Voiced Labialized (Nasal Bilabial) ->
      "mw"
    Consonant Voiced Unlabialized (Nasal Alveolar)->
      "n"
    Consonant Voiced Labialized (Nasal Alveolar)->
      "nw"
    Consonant Voiced Unlabialized (Nasal Velar) ->
      "ng"
    Consonant Voiced Labialized (Nasal Velar) ->
      "ngw"
    Consonant Voiceless Unlabialized (Fricative Centralized Bilabial) ->
      "wh"
    Consonant Voiced Unlabialized (Fricative Centralized Bilabial) ->
      "wh"
    Consonant Voiceless Unlabialized (Fricative Centralized Dental) ->
      "th"
    Consonant Voiced Unlabialized (Fricative Centralized Velar) ->
      "xh"
    Consonant Voiced Unlabialized (SibilantAffricate PostAlveolar) ->
      "tx"
    Vowel Unrounded Front Open Short ->
      "a"
    Vowel Unrounded Front CloseMid Short ->
      "e"
    Vowel Unrounded Front Close Short ->
      "i"
    Vowel Rounded Back CloseMid Short ->
      "o"
    Vowel Rounded Back Close Short ->
      "u"
    Vowel Rounded Front Close Short ->
      "y"
    Vowel Unrounded Central Mid Short ->
      "@"
    Vowel Unrounded Front Open Long ->
      "aa"
    Vowel Unrounded Front CloseMid Long ->
      "ee"
    Vowel Unrounded Front Close Long ->
      "ii"
    Vowel Rounded Back CloseMid Long ->
      "oo"
    Vowel Rounded Back Close Long ->
      "uu"
    Vowel Rounded Front Close Long ->
      "yy"
    Vowel Unrounded Central Mid Long ->
      "@@"
    _ ->
      "?"
