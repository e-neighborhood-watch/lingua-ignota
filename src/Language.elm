module Language
  exposing
    ( Language
    , generate
    )


import Random


import Extra.Random as Random
import Language.Fixing as Fixing
  exposing
    ( Fixing
    )
import Language.GrammaticalNumber as GramaticalNumber
  exposing
    ( GrammaticalNumber
    )
import Language.Marking.Verb as VerbMarking
  exposing
    ( VerbMarking
    )
import Language.Morpheme as Morpheme
import Language.MorphosyntacticAlignment as MorphosyntacticAlignment
  exposing
    ( MorphosyntacticAlignment
    )
import Language.Phoneme
  exposing
    ( Phoneme
    )
import Language.Phonotactics as Phonotactics
  exposing
    ( Phonotactics
    )
import Language.SentenceStructure as SentenceStructure
  exposing
    ( SentenceStructure
    )
import Language.SentenceStructure.Copula as CopulaStructure
  exposing
    ( CopulaStructure
    )


type alias Language =
  { phonotactics :
    Phonotactics
  , sentenceStructure :
    SentenceStructure
  , morphosyntacticAlignment :
    MorphosyntacticAlignment
  , grammaticalNumber :
    GrammaticalNumber
  , definiteIndefiniteDistinction :
    Bool
  , splitArticles :
    Bool
  , clusivity :
    Bool
  , reduplicatingEtymology :
    Bool
  , etymologySeed :
    Int
  , imperativeMarking :
    VerbMarking
  , adverbMarking :
    VerbMarking
  , adjectiveFixing :
    Fixing
  , determinerFixing :
    Fixing
  , compositionalPhraseFixing :
    Fixing
  , copulaStructure :
    CopulaStructure
  , baseSystem :
    Int
  , reverseDictionary :
    Int -> List Phoneme
  }


generate : Random.Generator Language
generate =
  let
    go : Phonotactics -> Random.Generator Language
    go phonotactics =
      Random.map
        ( Language phonotactics )
        SentenceStructure.generate
      |> Random.apTo MorphosyntacticAlignment.generate
      |> Random.apTo GramaticalNumber.generate
      |> Random.apTo ( Random.uniform True [False] )
      |> Random.apTo ( Random.uniform True [False] )
      |> Random.apTo ( Random.uniform True [False] )
      |> Random.apTo ( Random.uniform True [False] )
      |> Random.apTo ( Random.int Random.minInt Random.maxInt )
      |> Random.apTo VerbMarking.generate
      |> Random.apTo VerbMarking.generate
      |> Random.apTo Fixing.generate
      |> Random.apTo Fixing.generate
      |> Random.apTo Fixing.generate
      |> Random.apTo CopulaStructure.generate
      |> Random.apTo (Random.int 5 13)
      |> Random.apTo
        ( Random.map
          ( \ seed decoded ->
            seed
            + decoded
            |> Random.initialSeed
            |> Random.step (Morpheme.generate phonotactics.markovModel)
            |> Tuple.first
          )
          (Random.int Random.minInt Random.maxInt)
        )
  in
    Phonotactics.generate
    |> Random.andThen go
