module Extra.IsoDict exposing
  ( IsoDict
  , empty
  , get
  , insert
  , update
  , remove
  , toList
  )


import Dict exposing
  ( Dict
  )


type IsoDict c k v =
  IsoDict (Dict c ( k, v ))


empty : IsoDict c k v
empty = IsoDict Dict.empty


get : (k -> comparable) -> k -> IsoDict comparable k v -> Maybe v
get to key (IsoDict dict) =
  Dict.get (to key) dict
  |> Maybe.map Tuple.second


insert : (k -> comparable) -> k -> v -> IsoDict comparable k v -> IsoDict comparable k v
insert to key val (IsoDict dict) =
  Dict.insert (to key) (key, val) dict
  |> IsoDict


update : (k -> comparable) -> k -> (Maybe v -> Maybe v) -> IsoDict comparable k v -> IsoDict comparable k v
update to key f (IsoDict dict) =
  Dict.update (to key) (Maybe.map Tuple.second >> f >> Maybe.map (Tuple.pair key)) dict
  |> IsoDict


remove : (k -> comparable) -> k -> IsoDict comparable k v -> IsoDict comparable k v
remove to key (IsoDict dict) =
  Dict.remove (to key) dict
  |> IsoDict


toList : IsoDict comparable k v -> List ( k, v )
toList (IsoDict dict) =
  Dict.values dict
