module Extra.IsoDict.Int exposing
  ( IsoDict
  )


import Extra.IsoDict as IsoDict


type alias IsoDict k v = IsoDict.IsoDict Int k v
