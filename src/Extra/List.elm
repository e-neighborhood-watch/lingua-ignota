module Extra.List exposing
  ( find
  , delete
  )


find : (a -> Bool) -> List a -> Maybe a
find predicate list =
  case
    list
  of
    [] ->
      Nothing
    x :: xs ->
      if
        predicate x
      then
        Just x
      else
        find predicate xs


reverseAppend : List a -> List a -> List a
reverseAppend toRevAppend end =
  case
    toRevAppend
  of
    [] ->
      end
    last :: inits ->
      reverseAppend inits (last :: end)


delete : (a -> Bool) -> List a -> Maybe ( a, List a )
delete predicate =
  let
    go : List a -> List a -> Maybe ( a, List a )
    go inits rest =
      case
        rest
      of
        [] ->
          Nothing
        x :: xs ->
          if
            predicate x
          then
            Just
              ( x
              , reverseAppend inits xs
              )
          else
            go (x :: inits) xs
  in
    go []
