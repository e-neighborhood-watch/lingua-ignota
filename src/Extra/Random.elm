module Extra.Random
  exposing
    ( apTo
    , apFrom
    , mapM
    , join
    )


import Random


apTo :
  Random.Generator a
  -> Random.Generator (a -> b)
  -> Random.Generator b
apTo =
  Random.map2 (|>)


apFrom :
  Random.Generator (a -> b)
  -> Random.Generator a
  -> Random.Generator b
apFrom =
  Random.map2 (<|)


join : Random.Generator (Random.Generator a) -> Random.Generator a
join =
  Random.andThen identity


mapM :
  (a -> Random.Generator b) -> List a -> Random.Generator (List b)
mapM f xs =
  case
    xs
  of
    [] ->
      Random.constant []
    (y :: ys) ->
      Random.map2
        (::)
        (f y)
        (mapM f ys)
