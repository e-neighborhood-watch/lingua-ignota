module Extra.String exposing
  ( get
  )

get : Int -> String -> Maybe Char
get ix =
  String.slice ix (ix + 1)
  >> String.uncons
  >> Maybe.map Tuple.first
