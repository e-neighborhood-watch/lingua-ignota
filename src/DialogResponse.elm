module DialogResponse exposing
  ( DialogResponse (..)
  )

type DialogResponse
  = Yes
  | No
