module Quest exposing
  ( Quest
  , RawQuest (..)
  , translate
  , complete
  , isComplete
  , generate
  )


import Extra.Random as Random
import Map.Item as Item exposing
  ( Item
  )
import Language.Basic.Verb.Transitive as Transitive
import Language.Mood as Mood
import Language.NounPhrasal as NounPhrasal
import Language.Pronoun.Personal as PersonalPronoun
import Language.Sentence as Sentence exposing
  ( Sentence
  )
import Language.Verb.Ditransitive as Ditransitive
import Language.VerbPhrase as VerbPhrase
import Map.Inventory as Inventory exposing
  ( Inventory
  )
import Random


type RawQuest
  = GetMe Item Int
  | Trade Item Int Item Int


type alias Quest
  = { quest :
      RawQuest
    , overrideText :
      Maybe (List Sentence)
    }


generate : Random.Generator RawQuest
generate =
  let
    generateGetMe : Random.Generator RawQuest
    generateGetMe =
      Random.map2
        GetMe
        ( Random.uniform
          Item.Apple
          [ Item.Mushroom
          ]
        )
        (Random.int 1 5)
    generateTrade : Random.Generator RawQuest
    generateTrade =
      Random.uniform
        (Item.Apple, Item.Mushroom)
        [ (Item.Mushroom, Item.Apple)
        ]
      |> Random.andThen
        ( \ (item1, item2) ->
          Random.map
            ( Trade item1 )
            ( Random.int 1 5 )
          |> Random.apTo
            ( Random.constant
              item2
            )
          |> Random.apTo
            ( Random.int 2 6 )
        )
  in
    Random.uniform
      generateGetMe
      [ generateTrade
      ]
    |> Random.join


complete : RawQuest -> Inventory -> Inventory
complete quest inventory =
  case
    quest
  of
    GetMe item count ->
      Inventory.remove item count inventory
    Trade item1 count1 item2 count2 ->
      inventory
      |> Inventory.remove item1 count1
      |> Inventory.add item2 count2


isComplete : RawQuest -> Inventory -> Bool
isComplete quest inventory =
  case
    quest
  of
    GetMe item count ->
      Inventory.get item inventory >= count
    Trade item count _ _ ->
      Inventory.get item inventory >= count


translate : Inventory -> Quest -> List Sentence
translate inventory quest =
  case
    quest.overrideText
  of
    Just text ->
      text
    Nothing ->
      let
        item1 : Item
        item1 =
          case
            quest.quest
          of
            GetMe item _ ->
              item
            Trade item _ _ _ ->
              item

        count1 : Int
        count1 =
          case
            quest.quest
          of
            GetMe _ count ->
              count
            Trade _ count _ _ ->
              count

      in
        [ case
            quest.quest
          of
            Trade _ _ item2 count2 ->
              Sentence.Conditional
                Nothing
                ( VerbPhrase.Ditransitive
                  Ditransitive.Give
                  { subject =
                    ( NounPhrasal.Pronoun
                      PersonalPronoun.You
                    )
                  , directObject =
                    ( Item.translate item1 count1 )
                  , indirectObject =
                    ( NounPhrasal.Pronoun
                      PersonalPronoun.Me
                    )
                  }
                )
                Nothing
                ( VerbPhrase.Ditransitive
                  Ditransitive.Give
                  { subject =
                    ( NounPhrasal.Pronoun
                      PersonalPronoun.Me
                    )
                  , directObject =
                    ( Item.translate item2 count2 )
                  , indirectObject =
                    ( NounPhrasal.Pronoun
                      PersonalPronoun.You
                    )
                  }
                )
            GetMe _ _ ->
              Sentence.Sentence
                Mood.Indicative
                Nothing
                ( VerbPhrase.Transitive
                  Transitive.Need
                  { subject =
                    ( NounPhrasal.Pronoun
                      PersonalPronoun.Me
                    )
                  , directObject =
                    ( Item.translate item1 count1 )
                  }
                )
        ]
        ++
          if
            isComplete (GetMe item1 count1) inventory
          then
            [ Sentence.Sentence
              Mood.Interogative
              Nothing
              ( VerbPhrase.Ditransitive
                Ditransitive.Give
                { subject =
                  ( NounPhrasal.Pronoun
                    PersonalPronoun.You
                  )
                , directObject =
                  ( NounPhrasal.Pronoun
                    ( case
                        count1
                      of
                        1 ->
                          PersonalPronoun.Her
                        2 ->
                          PersonalPronoun.HerHer
                        _ ->
                          PersonalPronoun.Them
                    )
                  )
                , indirectObject =
                  ( NounPhrasal.Pronoun
                    PersonalPronoun.Me
                  )
                }
              )
            ]
          else
            [ ]
