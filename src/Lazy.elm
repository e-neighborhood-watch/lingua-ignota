module Lazy exposing
  ( Lazy
  , map
  , map2
  )

type alias Lazy a =
  () -> a

map : (a -> b) -> Lazy a -> Lazy b
map f g x =
  f (g x)

map2 : (a -> b -> c) -> Lazy a -> Lazy b -> Lazy c
map2 f g h x =
  f (g x) (h x)
