module Main exposing
  ( main
  )


import Array
import Browser
import Browser.Events
import Curses.Test3 as Curses
import DialogResponse exposing
  ( DialogResponse (..)
  )
import Dict exposing (..)
import Init
import Language
  exposing
    ( Language
    )
import Map
  exposing
    ( Map
    )
import Message
  exposing
    ( Message
    )
import Model
  exposing
    ( Model
    )
import Quest
import Random
import Html exposing (Html)
import Html.Attributes
import Json.Decode
import Task exposing (Task)


screenRows : Int
screenRows =
  35


screenCols : Int
screenCols =
  80


init : a -> ( Model, Cmd Message )
init =
  Init.init


generateTileData : Int -> Int -> Curses.TileAtlas -> Maybe Language -> Map -> Task Curses.TileDataError Curses.TileData
generateTileData rows cols tileAtlas maybeLanguage map =
  Map.render rows cols maybeLanguage map
  |> ( \ tiles -> { tiles = tiles, rows = rows, cols = cols } )
  |> Curses.generateTileData tileAtlas


update : Message -> Model -> ( Model, Cmd Message )
update msg prev =
  case
    msg
  of
    Message.DialogResponse response ->
      let
        mapAndSeed : ( Map, Random.Seed )
        mapAndSeed =
          prev.seed
          |> Maybe.withDefault (Random.initialSeed 8)
          |>
            Random.step
              (Map.updateQuest response prev.map)
      in
        case
          prev.tileAtlas
        of
          Nothing ->
            ( { prev
              | map =
                Tuple.first mapAndSeed
              , seed =
                mapAndSeed
                |> Tuple.second
                |> Just
              }
            , Cmd.none
            )
          Just tileAtlas ->
            ( { prev
              | map =
                Tuple.first mapAndSeed
              , seed =
                mapAndSeed
                |> Tuple.second
                |> Just
              , nextFrame = prev.nextFrame + 1
              }
            , mapAndSeed
              |> Tuple.first
              |> generateTileData screenRows screenCols tileAtlas prev.language
              |> Task.attempt (Message.TileDataLoaded prev.nextFrame)
            )
    Message.Move direction ->
      let
        newMap : Maybe Map
        newMap =
          case
            direction
          of
            Message.Left ->
              Map.movePlayer -1 0 prev.map
            Message.Right ->
              Map.movePlayer 1 0 prev.map
            Message.Up ->
              Map.movePlayer 0 -1 prev.map
            Message.Down ->
              Map.movePlayer 0 1 prev.map
      in
        case
          newMap
        of
          Nothing ->
            ( prev
            , Cmd.none
            )
          Just map ->
            case
              prev.tileAtlas
            of
              Nothing ->
                ( { prev
                  | map =
                    map
                  }
                , Cmd.none
                )
              Just tileAtlas ->
                ( { prev
                  | nextFrame =
                    prev.nextFrame + 1
                  , map =
                    map
                  }
                , generateTileData screenRows screenCols tileAtlas prev.language map
                  |> Task.attempt (Message.TileDataLoaded prev.nextFrame)
                )
    Message.TileAtlasLoaded result ->
      case
        result
      of
        Err _ ->
          ( prev
          , Cmd.none
          )
        Ok tileAtlas ->
          ( { prev
            | tileAtlas =
              Just tileAtlas
            , nextFrame =
              prev.nextFrame + 1
            }
          , generateTileData screenRows screenCols tileAtlas prev.language prev.map
            |> Task.attempt (Message.TileDataLoaded prev.nextFrame)
          )
    Message.TileDataLoaded frame result ->
      ( if
          frame <= prev.currentFrame
        then
          prev
        else
          case
            result
          of
            Err _ ->
              prev
            Ok tileData ->
              { prev
              | generatedTileData =
                Just tileData
              , currentFrame =
                frame
              }
      , Cmd.none
      )
    Message.LanguageAndSeedGenerated ( language, seed ) ->
      case
        prev.tileAtlas
      of
        Nothing ->
          ( { prev
            | language = Just language
            , seed = Just seed
            }
          , Cmd.none
          )
        Just tileAtlas ->
          ( { prev
            | language =
              Just language
            , seed =
              Just seed
            , nextFrame =
              prev.nextFrame + 1
            }
          , generateTileData screenRows screenCols tileAtlas (Just language) prev.map
            |> Task.attempt (Message.TileDataLoaded prev.nextFrame)
          )
    Message.Noop ->
      ( prev
      , Cmd.none
      )


subscriptions : Model -> Sub Message
subscriptions _ =
  let
    keyToMessage : String -> Message
    keyToMessage key =
      case
        key
      of
        "ArrowLeft" ->
          Message.Move Message.Left
        "ArrowRight" ->
          Message.Move Message.Right
        "ArrowUp" ->
          Message.Move Message.Up
        "ArrowDown" ->
          Message.Move Message.Down
        "y" ->
          Message.DialogResponse Yes
        "Y" ->
          Message.DialogResponse Yes
        "n" ->
          Message.DialogResponse No
        "N" ->
          Message.DialogResponse No
        _ ->
          Message.Noop
  in
    Json.Decode.string
    |> Json.Decode.field "key"
    |> Json.Decode.map keyToMessage
    |> Browser.Events.onKeyDown


view : Model -> Browser.Document Message
view { generatedTileData } =
  let
    styleString : String
    styleString =
      """
        body {
          display: flex;
          justify-content: center;
          background-color: black;
        }
      """
    style : Html Message
    style =
      Html.node
        "style"
        []
        [ Html.text styleString
        ]
  in
    case
      generatedTileData
    of
      Nothing ->
        { title =
          "Loading..."
        , body =
          [ style
          ]
        }
      Just tileData ->
        { title =
          "LINGVAIGNOTA"
        , body =
          [ style
          , Curses.render tileData
          ]
        }


main : Program () Model Message
main =
  Browser.document
    { init = init
    , update = update
    , subscriptions = subscriptions
    , view = view
    }
