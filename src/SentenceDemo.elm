module SentenceDemo exposing (main)


import Browser
import Html exposing (Html)
import Html.Events
import Random


import Extra.Random as Random
import Language.Phonotactics as Phonotactics
import Language.Phoneme exposing
  ( Phoneme
  )
import Language exposing
  ( Language
  )
import Language.Noun
import Language.NounPhrasal
import Language.Ontology
import Language.Orthography as Orthography
import Language.Sentence as Sentence exposing
  ( Sentence
  )


type Messages
  = NewWord
  | NewLanguage
  | SentencesGenerated (List Sentence)
  | LanguageGenerated Language


type alias Model =
  { sentences : List Sentence
  , language : Maybe Language
  , text : String
  }


newSentences : Cmd Messages
newSentences =
  Random.constant Sentence.tests
  |> Random.generate SentencesGenerated


newLanguage : Cmd Messages
newLanguage =
  Language.generate
  |> Random.generate LanguageGenerated


sentencesToString : Language -> List Sentence -> String
sentencesToString lang =
  List.map (Sentence.translate lang)
  >> List.map
    (( List.map (List.map Orthography.test >> String.concat)
    >> String.join " "
    ))
  >> String.join ". "


init : ( Model, Cmd Messages )
init =
  ( { sentences =
      []
    , language =
      Nothing
    , text =
      ""
    }
  , Cmd.batch
      [ newSentences
      , newLanguage
      ]
  )


update : Messages -> Model -> (Model, Cmd Messages)
update msg old =
  case
    msg
  of
    NewWord ->
      ( old
      , newSentences
      )
    NewLanguage ->
      ( old
      , newLanguage
      )
    LanguageGenerated lang ->
      ( { old
        | language =
          Just lang
        , text =
          sentencesToString lang old.sentences
        }
      , Cmd.none
      )
    SentencesGenerated sentences ->
      ( { old
        | sentences =
          sentences
        , text =
          case
            old.language
          of
            Nothing ->
              old.text
            Just lang ->
              sentencesToString lang sentences
        }
      , Cmd.none
      )


view : Model -> Html Messages
view { text } =
  Html.div
    []
    [ Html.button
      [ Html.Events.onClick NewWord
      ]
      [ Html.text "New Word!"
      ]
    , Html.button
      [ Html.Events.onClick NewLanguage
      ]
      [ Html.text "New Language!"
      ]
    , Html.p
      []
      [ Html.text text
      ]
    ]


main : Program () Model Messages
main =
  Browser.element
    { init = always init
    , update = update
    , view = view
    , subscriptions = always Sub.none
    }
