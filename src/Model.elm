module Model
  exposing
    ( Model
    )


import Curses.Test3 as Curses
import Language
  exposing
    ( Language
    )
import Map
  exposing
    ( Map
    )
import Random


type alias Model =
  { generatedTileData :
    Maybe Curses.TileData
  , tileAtlas :
    Maybe Curses.TileAtlas
  , ipaAtlas :
    Maybe Curses.TileAtlas
  , currentFrame :
    Int
  , nextFrame :
    Int
  , map :
    Map
  , language :
    Maybe Language
  , seed :
    Maybe Random.Seed
  }

