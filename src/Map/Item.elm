module Map.Item
  exposing
    ( Item (..)
    , toInt
    , translate
    , render
    )


import Curses.Test3 as Curses
import Language.Basic.Noun as Noun exposing
  ( Noun
  )
import Language.Determiner as Determiner
import Language.NounPhrasal as NounPhrasal exposing
  ( NounPhrasal
  )
import Language.Number as Number
import Language.Particle.Number as NumberParticle


type Item
  = Apple
  | Mushroom
  | ApplePie


toInt : Item -> Int
toInt item =
  case
    item
  of
    Apple ->
      0
    Mushroom ->
      1
    ApplePie ->
      2


translate : Item -> Int -> NounPhrasal
translate item count =
  let
    word : Noun
    word =
      case
        item
      of
        Apple ->
          Noun.Apple
        Mushroom ->
          Noun.Mushroom
        ApplePie ->
          Noun.ApplePie
  in
    case
      count
    of
      1 ->
        ( NounPhrasal.Noun
          word
          []
          ( Determiner.Article
            False
            NumberParticle.Singular
          )
          Nothing
        )
      _ ->
        ( NounPhrasal.NumberPhrase
          ( Number.Number count )
          ( NounPhrasal.Noun
            word
            []
            ( Determiner.Article
              False
              ( case
                  count
                of
                  2 ->
                    NumberParticle.Dual
                  _ ->
                    NumberParticle.Plural
              )
            )
            Nothing
          )
        )


render : Item -> Curses.Tile
render item =
  case
    item
  of
    Apple ->
      { fgColor =
        0xFF0000
      , bgColor =
        0x000000
      , tile =
        ( 0
        , 7
        )
      }
    Mushroom ->
      { fgColor =
        0x660066
      , bgColor =
        0x000000
      , tile =
        ( 0
        , 6
        )
      }
    ApplePie ->
      { fgColor =
        0xFF0000
      , bgColor =
        0x000000
      , tile =
        ( 0
        , 8
        )
      }

