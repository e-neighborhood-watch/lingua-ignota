module Map.Room exposing
  ( Room
  , room
  , layer
  , tileAt
  )


import Curses.Test3 as Curses
import Map.Room.Tile as Tile exposing
  ( Tile
  )
import Map.Room.Wall as Wall


type Room =
  Room
    { top : Int
    , bottom : Int
    , left : Int
    , right : Int
    , door : ( Int, Int )
    }


room : { bottom : Int, left : Int, top : Int, right : Int } -> ( Int, Int ) -> Room
room { bottom, left, top, right } door =
  Room
    { bottom = bottom
    , left = left
    , top = top
    , right = right
    , door = door
    }


layer : Int -> Int -> ( Int, Int ) -> List Room -> Tile
layer x y playerLoc =
  List.map (tileAt x y playerLoc)
  >> Tile.layer


tileAt : Int -> Int -> ( Int, Int ) -> Room -> Tile
tileAt x y ( playerX, playerY ) (Room theRoom) =
  let
    playerNotInside : Bool
    playerNotInside =
      playerX <= theRoom.left
      || theRoom.right <= playerX
      || playerY <= theRoom.top
      || theRoom.bottom <= playerY
    playerNotOutside : Bool
    playerNotOutside =
      theRoom.left <= playerX
      && playerX <= theRoom.right
      && theRoom.top <= playerY
      && playerY <= theRoom.bottom
  in
    if
      ( x, y ) == theRoom.door
    then
      Tile.Door playerNotOutside
    else
      if
        theRoom.top <= y
        && y <= theRoom.bottom
        && theRoom.left <= x
        && x <= theRoom.right
        && (( y == theRoom.top
           || y == theRoom.bottom
           || x == theRoom.left
           || x == theRoom.right
           ))
      then
        Tile.Wall
          { shape =
            { north =
              (( x == theRoom.left
              || x == theRoom.right
              ))
              && y /= theRoom.top
            , south =
              (( x == theRoom.left
              || x == theRoom.right
              ))
              && y /= theRoom.bottom
            , east =
              (( y == theRoom.top
              || y == theRoom.bottom
              ))
              && x /= theRoom.right
            , west =
              (( y == theRoom.top
              || y == theRoom.bottom
              ))
              && x /= theRoom.left
            }
          , interior =
            playerNotOutside
          , interiorDirection =
            case
              ( ( x == theRoom.left
                , x == theRoom.right
                )
              , ( y == theRoom.top
                , y == theRoom.bottom
                )
              )
            of
              ((True, False), (False, False)) ->
                Just Wall.East
              ((False, True), (False, False)) ->
                Just Wall.West
              ((False, False), (True, False)) ->
                Just Wall.South
              ((False, False), (False, True)) ->
                Just Wall.North
              _ ->
                Nothing
          }
      else
        if
          theRoom.top < y
          && y < theRoom.bottom
          && theRoom.left < x
          && x < theRoom.right
        then
          if
            playerNotOutside
          then
            Tile.Floor
          else
            Tile.Hidden
        else
          if
            playerNotInside
          then
            Tile.Ground
          else
            Tile.Hidden
