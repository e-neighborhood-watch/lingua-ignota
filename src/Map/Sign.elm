module Map.Sign exposing
  ( Sign
  , render
  )

import Curses.Test3 as Curses
import Map.Text exposing
  ( Text
  )


type alias Sign =
  Text


render : Sign -> Curses.Tile
render _ =
  { fgColor =
    0x964b00
  , bgColor =
    0x000000
  , tile =
    ( 6
    , 17
    )
  }
