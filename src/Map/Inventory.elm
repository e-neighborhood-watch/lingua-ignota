module Map.Inventory exposing
  ( Inventory
  , empty
  , add
  , get
  , pickUpMessage
  , render
  , remove
  )


import Array exposing
  ( Array
  )
import Curses.Test3 as Curses
import Extra.IsoDict as IsoDict
import Extra.IsoDict.Int exposing
  ( IsoDict
  )
import Extra.String as String
import Language exposing
  ( Language
  )
import Language.Basic.Verb.Transitive as TransitiveVerb
import Language.Mood as Mood
import Language.NounPhrasal as NounPhrasal
import Language.Orthography as Orthography
import Language.Pronoun.Personal as PersonalPronoun
import Language.Sentence as Sentence exposing
  ( Sentence
  )
import Language.VerbPhrase as VerbPhrase
import Map.Item as Item exposing
  ( Item
  )
import Map.Text as Text


type Inventory =
  Inventory (IsoDict Item Int)


empty : Inventory
empty =
  Inventory
    IsoDict.empty


add : Item -> Int -> Inventory -> Inventory
add item num (Inventory dict) =
  IsoDict.update Item.toInt item (Maybe.withDefault 0 >> (+) num >> Just) dict
  |> Inventory

get : Item -> Inventory -> Int
get item (Inventory dict) =
  IsoDict.get Item.toInt item dict
  |> Maybe.withDefault 0

remove : Item -> Int -> Inventory -> Inventory
remove item count (Inventory dict) =
  let
    reduce : Int -> Maybe Int
    reduce n =
      if
        n < 1
      then
        Nothing
      else
        Just n
  in
    IsoDict.update Item.toInt item (Maybe.withDefault 0 >> (+) (negate count) >> reduce) dict
    |> Inventory

pickUpMessage : Item -> Int -> Sentence
pickUpMessage item count =
  Sentence.Sentence
    Mood.Indicative
    Nothing
    ( VerbPhrase.Transitive
      TransitiveVerb.PickUp
      { subject =
        ( NounPhrasal.Pronoun
          PersonalPronoun.You
        )
      , directObject =
        ( Item.translate item count )
      }
    )


render : Int -> Int -> Language -> Inventory -> Int -> Int -> Curses.Tile
render rows cols language (Inventory dict) =
  let
    itemLine : ( Item, Int ) -> String
    itemLine ( item, num ) =
      let
        itemWords : List String
        itemWords =
          Item.translate item num
          |> NounPhrasal.translate language
          |>
            List.map
              ( List.map Orthography.ipa
                >> String.concat
              )

        fitItem : List String -> Int -> String -> String
        fitItem remWords usedLength acc =
          case
            remWords
          of
            [] ->
              acc
            nextWord :: rest ->
              let
                nextLength : Int
                nextLength = usedLength + 1 + String.length nextWord
              in
                if
                  nextLength
                  > cols - 4
                then
                  acc ++ " ..."
                else
                  fitItem rest nextLength (acc ++ " " ++ nextWord)
      in
        case
          itemWords
        of
          [] ->
            ""
          firstWord :: rest ->
            let
              wordLength : Int
              wordLength = String.length firstWord
            in
              if
                wordLength > cols
              then
                String.left (cols - 3) firstWord
                ++ "..."
              else
                let
                  fullString : String
                  fullString = String.join " " itemWords
                in
                  if
                    String.length fullString
                    <= cols
                  then
                    fullString
                  else
                    fitItem rest wordLength firstWord
    lines : Array String
    lines =
      let
        allLines : Array String
        allLines =
          IsoDict.toList dict
          |> List.map itemLine
          |> Array.fromList
      in
        if
          Array.length allLines
          > rows
        then
          Array.set (rows - 1) "..." allLines
        else
          allLines
    go : Int -> Int -> Curses.Tile
    go row col =
      lines
      |> Array.get row
      |>
        Maybe.andThen
          ( String.get col
            >>
              Maybe.map
                ( Text.charToTile
                  >>
                    \ tile ->
                      { fgColor = 0xFFFFFF
                      , bgColor = 0x000000
                      , tile = tile
                      }
                )
          )
      |>
        Maybe.withDefault
          { fgColor = 0x000000
          , bgColor = 0x000000
          , tile = ( 0, 0 )
          }
  in
    go
