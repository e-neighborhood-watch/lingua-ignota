module Map.Text
  exposing
    ( Text (..)
    , render
    , charToTile
    )

import Array
  exposing
    ( Array
    )
import Curses.Test3 as Curses
import Extra.String as String
import Language
  exposing
    ( Language
    )
import Language.NounPhrasal as NounPhrasal
  exposing
    ( NounPhrasal
    )
import Language.Orthography as Orthography
import Language.Phoneme
  exposing
    ( Phoneme
    )
import Language.Sentence as Sentence
  exposing
    ( Sentence
    )


type Text
  = Sentences (List Sentence)
  | Label NounPhrasal


asciiToUpper : String -> String
asciiToUpper =
  String.map
    ( \ char ->
      if
        Char.toCode char < 0x80
      then
        Char.toUpper char
      else
        char
    )

capitalizeFirstLetter : List String -> List String
capitalizeFirstLetter words =
  case
    words
  of
    [] ->
      []
    first :: rest ->
      ( if
          String.startsWith "'" first
        then
          asciiToUpper
            ( String.left 2 first )
          ++ String.dropLeft 2 first
        else
          asciiToUpper
            ( String.left 1 first )
          ++ String.dropLeft 1 first
      )
      :: rest


translateSentence : Language -> Sentence -> List String
translateSentence language sentence =
  let
    words : List String
    words =
      Sentence.translate language sentence
      |> List.map
         ( List.map Orthography.ipa >> String.concat)
  in
    case
      List.reverse words
    of
      [] ->
        []
      last :: inits ->
        ( if
            Sentence.isQuestion sentence
          then
            last ++ "?"
          else
            last ++ "."
        )
        :: inits
        |> List.reverse
        |> capitalizeFirstLetter


translateSentences : Language -> List Sentence-> List String
translateSentences lang =
  List.concatMap (translateSentence lang)


translateLabel : Language -> NounPhrasal -> List String
translateLabel lang label =
  NounPhrasal.translate lang label
  |> List.map
    (( List.map Orthography.ipa
    >> String.concat
    >> asciiToUpper
    ))


render : Int -> Int -> Language -> Text -> Int -> Int -> Curses.Tile
render rows cols language text =
  let
    textByWords : List String
    textByWords =
      case
        text
      of
        Sentences sentences ->
          translateSentences language sentences
        Label label ->
          translateLabel language label


    lines : Array String
    lines =
      let
        goLines : Int -> Int -> List String -> List String -> List String
        goLines row col words acc =
          case
            words
          of
            [] ->
              acc
            word :: rest ->
              if
                row >= rows
              then
                acc
              else
                let
                  wordLength : Int
                  wordLength = String.length word
                in
                  if
                    row == rows - 1
                  then
                    if
                      wordLength > cols
                    then
                      "..." :: acc
                    else
                      let
                        remainingString : String
                        remainingString = String.join " " rest
                      in
                        if
                          String.length remainingString <= cols
                        then
                          remainingString :: acc
                        else
                          let
                            buildFitString : Int -> List String -> String -> String
                            buildFitString curLength remWords fitAcc =
                              case
                                remWords
                              of
                                [] ->
                                  fitAcc
                                nextWord :: remFit ->
                                  let
                                    newLength : Int
                                    newLength = curLength + 1 + String.length nextWord
                                  in
                                    if
                                      newLength > cols - 4
                                    then
                                      fitAcc ++ " ..."
                                    else
                                      buildFitString newLength remFit (fitAcc ++ " " ++ nextWord)
                            in
                              buildFitString wordLength rest word :: acc
                  else
                    if
                      col == 0
                    then
                      if
                        wordLength > cols
                      then
                        goLines (row+1) 0 rest ((String.left (cols - 3) word ++ "...") :: acc)
                      else
                        goLines row wordLength rest (word :: acc)
                    else
                      if
                        wordLength + 1 + col > cols
                      then
                        goLines (row+1) 0 (word :: rest) acc
                      else
                        case
                          acc
                        of
                          [] ->
                            goLines row (col + wordLength + 1) rest [" " ++ word]
                          curLine :: restLines ->
                            goLines row (col + wordLength + 1) rest ((curLine ++ " " ++ word) :: restLines)
      in
        goLines 0 0 textByWords []
        |> List.reverse
        |> Array.fromList
    go : Int -> Int -> Curses.Tile
    go row col =
      Array.get row lines
      |>
        Maybe.andThen
          ( String.get col
            >> Maybe.map charToTile
          )
      |> Maybe.withDefault ( 0, 0 )
      |> ( \ tile ->
           { fgColor =
             0xFFFFFF
           , bgColor =
             0x000000
           , tile =
             tile
           }
         )
  in
    go


charToTile : Char -> ( Int, Int )
charToTile char =
  let
    code : Int
    code = Char.toCode char
  in
    if
      0x20 <= code
      && code < 0x200
    then
      ( code // 32, modBy 32 code )
    else
      ( 1, 31 )
