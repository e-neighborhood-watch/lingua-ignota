module Map.Decoration exposing
  ( Decoration (..)
  )

import Curses.Test3 as Curses

type Decoration
  = Decoration
    Curses.Tile
    Bool
