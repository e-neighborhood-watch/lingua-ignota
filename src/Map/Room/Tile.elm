module Map.Room.Tile exposing
  ( Tile (..)
  , layer
  , render
  )


import Curses.Test3 as Curses
import Map.Room.Wall as Wall exposing
  ( Wall
  )

type Tile
  = Ground
  | Floor
  | Door Bool
  | Wall Wall
  | Hidden


layer : List Tile -> Tile
layer = List.foldl merge Ground


merge : Tile -> Tile -> Tile
merge tile1 tile2 =
  case
    tile1
  of
    Hidden ->
      Hidden
    Door interior ->
      case
        tile2
      of
        Hidden ->
          if
            interior
          then
            Door interior
          else
            Hidden
        _ ->
          Door interior
    Wall cxns ->
      case
        tile2
      of
        Hidden ->
          if
            cxns.interior
          then
            Wall cxns
          else
            Hidden
        Door interior ->
          Door interior
        Wall cxns2 ->
          Wall.merge cxns cxns2
          |> Wall
        _ ->
          Wall cxns
    Floor ->
      Floor
    Ground ->
      tile2


render : Tile -> Curses.Tile
render tile =
  case
    tile
  of
    Ground ->
      { fgColor = 0x228B22
      , bgColor = 0x000000
      , tile = ( 1, 14 )
      }
    Hidden ->
      { fgColor = 0x000000
      , bgColor = 0x000000
      , tile = ( 0, 0 )
      }
    Floor ->
      { fgColor = 0xEFEFEF
      , bgColor = 0x000000
      , tile = ( 1, 14 )
      }
    Door _ ->
      { fgColor = 0x964B00
      , bgColor = 0x000000
      , tile = ( 1 , 11 )
      }
    Wall { shape, interior } ->
      { fgColor =
        0xEFEFEF
      , bgColor =
        0x000000
      , tile =
        case
          ( (shape.north
            , shape.south
            )
          , (shape.east
            ,shape.west
            )
          )
        of
          ((True, True), (True, True)) ->
            ( 6, 14 )
          ((True, False), (True, True)) ->
            ( 6, 10 )
          ((True, True), (True, False)) ->
            ( 6, 12 )
          ((False, True), (True, True)) ->
            ( 6, 11 )
          ((True, True), (False, True)) ->
            ( 5, 25 )
          ((False, False), (True, True)) ->
            ( 6, 13 )
          ((True, True), (False, False)) ->
            ( 5, 26 )
          ((True, False), (False, True)) ->
            ( 5, 28 )
          ((True, False), (True, False)) ->
            ( 6, 8 )
          ((False, True), (True, False)) ->
            ( 6, 9 )
          ((False, True), (False, True)) ->
            ( 5, 27 )
          ((False, False), (False, False)) ->
            ( 1, 14 )
          _ ->
            ( 1, 31 )
      }
