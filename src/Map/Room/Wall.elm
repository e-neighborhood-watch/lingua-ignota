module Map.Room.Wall exposing
  ( BasicDirection (..)
  , WallShape
  , Wall
  , merge
  )

type BasicDirection
  = North
  | South
  | East
  | West

type alias WallShape =
  { north :
    Bool
  , south :
    Bool
  , east :
    Bool
  , west :
    Bool
  }

type alias Wall =
  { shape :
    WallShape
  , interior :
    Bool
  , interiorDirection :
    Maybe BasicDirection
  }


setWallDirection : BasicDirection -> Bool -> WallShape -> WallShape
setWallDirection direction new shape =
  case
    direction
  of
    North ->
      { shape
      | north =
        new
      }
    South ->
      { shape
      | south =
        new
      }
    East ->
      { shape
      | east =
        new
      }
    West ->
      { shape
      | west =
        new
      }


merge : Wall -> Wall -> Wall
merge addedWall oldWall =
  { shape =
    { north =
      oldWall.shape.north || addedWall.shape.north
    , south =
      oldWall.shape.south || addedWall.shape.south
    , east =
      oldWall.shape.east || addedWall.shape.east
    , west =
      oldWall.shape.west || addedWall.shape.west
    }
    |>
      case
        addedWall.interiorDirection
      of
        Nothing ->
          identity
        Just dir ->
          setWallDirection dir False
  , interior =
    addedWall.interior
    || oldWall.interior
  , interiorDirection =
    addedWall.interiorDirection
  }
