module Map.NonPlayerCharacter exposing
  ( NonPlayerCharacter
  , Speech (..)
  , nonPlayerCharacter
  , render
  , at
  , listen
  , getQuest
  , setQuest
  )


import Curses.Test3 as Curses
import Language.Sentence exposing
  ( Sentence
  )
import Map.Inventory as Inventory exposing
  ( Inventory
  )
import Map.Text as Text exposing
  ( Text
  )
import Quest

type Speech
  = Quest Quest.Quest
  | Info (List Sentence)

type NonPlayerCharacter
  = NonPlayerCharacter
    { x :
      Int
    , y :
      Int
    , color :
      Int
    , speech :
      Speech
    }


render : NonPlayerCharacter -> Curses.Tile
render ( NonPlayerCharacter { color } ) =
  { fgColor =
    color
  , bgColor =
    0x000000
  , tile =
    ( 0, 1 )
  }


at : Int -> Int -> NonPlayerCharacter -> Bool
at tx ty (NonPlayerCharacter { x, y }) =
  tx == x && ty == y


listen : Inventory -> NonPlayerCharacter -> Text
listen inventory (NonPlayerCharacter { speech }) =
  case
    speech
  of
    Quest quest ->
      Quest.translate inventory quest
      |> Text.Sentences
    Info sentences ->
      sentences
      |> Text.Sentences


getQuest : NonPlayerCharacter -> Maybe Quest.Quest
getQuest (NonPlayerCharacter { speech }) =
  case
    speech
  of
    Info _ ->
      Nothing
    Quest quest ->
      Just quest


setQuest : Quest.Quest -> NonPlayerCharacter -> NonPlayerCharacter
setQuest newQuest (NonPlayerCharacter npc) =
  { npc
  | speech =
    Quest newQuest
  }
  |> NonPlayerCharacter


nonPlayerCharacter : { x : Int, y : Int } -> Int -> Speech -> NonPlayerCharacter
nonPlayerCharacter { x, y } color speech =
  NonPlayerCharacter
    { x =
      x
    , y =
      y
    , color =
      color
    , speech =
      speech
    }
