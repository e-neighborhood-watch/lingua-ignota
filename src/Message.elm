module Message
  exposing
    ( Message (..)
    , Direction (..)
    )


import Curses.Test3 as Curses
import DialogResponse
  exposing
    ( DialogResponse (..)
    )
import Language
  exposing
    ( Language
    )
import Random


type Direction
  = Up
  | Down
  | Left
  | Right


type Message
  = Noop
  | Move Direction
  | DialogResponse DialogResponse
  | TileAtlasLoaded (Result Curses.LoadAtlasError Curses.TileAtlas)
  | TileDataLoaded Int (Result Curses.TileDataError Curses.TileData)
  | LanguageAndSeedGenerated ( Language, Random.Seed )
