module Init
  exposing
    ( init
    )


import Curses.Test3 as Curses
import Dict
  exposing
    ( Dict
    )
import Language
  exposing
    ( Language
    )
import Language.Basic.Noun as Noun
import Language.Verb.Ditransitive as DitransitiveVerb
import Language.Basic.Verb.Transitive as TransitiveVerb
import Language.Determiner as Determiner
import Language.Mood as Mood
import Language.NounPhrasal as NounPhrasal
import Language.Number as Number
import Language.Particle.Number as NumberParticle
import Language.Pronoun.Personal as PersonalPronoun
import Language.Sentence as Sentence
import Language.VerbPhrase as VerbPhrase
import Map
import Map.Decoration as Decoration
  exposing
    ( Decoration (..)
    )
import Map.Item as Item
  exposing
    ( Item
    )
import Map.NonPlayerCharacter as NonPlayerCharacter
  exposing
    ( NonPlayerCharacter
    )
import Map.Room
  exposing
    ( Room
    )
import Map.Text as Text
  exposing
    ( Text
    )
import Message
  exposing
    ( Message
    )
import Model
  exposing
    ( Model
    )
import Quest
import Random
import Task
  exposing
    ( Task
    )


rooms : List Room
rooms =
  [ Map.Room.room
    { top = -10
    , bottom = -5
    , left = -5
    , right = 6
    }
    ( -5, -7 )
  , Map.Room.room
    { top = -18
    , bottom = -12
    , left = -12
    , right = 1
    }
    ( -8, -12 )
  , Map.Room.room
    { top = -8
    , bottom = -5
    , left = 5
    , right = 10
    }
    ( 5, -6 )
  , Map.Room.room
    { top = -33
    , bottom = -29
    , left = 9
    , right = 23
    }
    ( 16, -29 )
  , Map.Room.room
    { top = -15
    , bottom = -7
    , left = 4
    , right = 11
    }
    ( 4, -9 )
  , Map.Room.room
    { top = -19
    , bottom = -14
    , left = -4
    , right = 12
    }
    ( 2, -14 )
  , Map.Room.room
    { top = -33
    , bottom = -24
    , left = 22
    , right = 27
    }
    ( 22, -30 )
  ]


npcs : List NonPlayerCharacter
npcs =
  [ NonPlayerCharacter.nonPlayerCharacter
    { x = 20
    , y = -28
    }
    0xEF0000
    ( NonPlayerCharacter.Quest
      { quest =
        ( Quest.Trade
          Item.ApplePie
          1
          Item.Mushroom
          5
        )
      , overrideText =
        Just
          [ Sentence.Sentence
            Mood.Indicative
            Nothing
            ( VerbPhrase.Transitive
              TransitiveVerb.Want
              { subject =
                ( NounPhrasal.Pronoun
                  PersonalPronoun.Me
                )
              , directObject =
                ( NounPhrasal.Noun
                  Noun.ApplePie
                  [
                  ]
                  ( Determiner.Article
                    False
                    NumberParticle.Singular
                  )
                  Nothing
                )
              }
            )
          , Sentence.Conditional
            Nothing
            ( VerbPhrase.Ditransitive
              DitransitiveVerb.Give
              { subject =
                ( NounPhrasal.Pronoun
                  PersonalPronoun.You
                )
              , directObject =
                ( NounPhrasal.NumberPhrase
                  ( Number.Number 5 )
                  ( NounPhrasal.Noun
                    Noun.Apple
                    [
                    ]
                    ( Determiner.Article
                      False
                      NumberParticle.Plural
                    )
                    Nothing
                  )
                )
              , indirectObject =
                ( NounPhrasal.Noun
                  Noun.Baker
                  [
                  ]
                  ( Determiner.Article
                    True
                    NumberParticle.Singular
                  )
                  Nothing
                )
              }
            )
            Nothing
            ( VerbPhrase.Ditransitive
              DitransitiveVerb.Give
              { subject =
                ( NounPhrasal.Pronoun
                  PersonalPronoun.Her
                )
              , directObject =
                ( NounPhrasal.Noun
                  Noun.ApplePie
                  [
                  ]
                  ( Determiner.Article
                    False
                    NumberParticle.Singular
                  )
                  Nothing
                )
              , indirectObject =
                ( NounPhrasal.Pronoun
                  PersonalPronoun.You
                )
              }
            )
          ]
      }
    )
  , NonPlayerCharacter.nonPlayerCharacter
    { x = -9
    , y = -8
    }
    0x00EF00
    ( NonPlayerCharacter.Quest
      { quest =
        ( Quest.Trade
          Item.Apple
          1
          Item.Mushroom
          1
        )
      , overrideText =
        Nothing
      }
    )
  , NonPlayerCharacter.nonPlayerCharacter
    { x = 4
    , y = -17
    }
    0x00EF66
    ( NonPlayerCharacter.Quest
      { quest =
        ( Quest.Trade
          Item.Apple
          5
          Item.Mushroom
          5
        )
      , overrideText =
        Nothing
      }
    )
  ]


signs : Dict ( number, number1 ) Text
signs =
  ( Dict.fromList
    [ ( (-7, -9)
      , ( Text.Label
          ( NounPhrasal.Noun
            Noun.Bakery
            []
            ( Determiner.Article
              True
              NumberParticle.Singular
            )
            Nothing
          )
        )
      )
    , ( (14, -21)
      , ( Text.Label
          ( NounPhrasal.Noun
            Noun.Orchard
            []
            ( Determiner.Article
              True
              NumberParticle.Singular
            )
            Nothing
          )
        )
      )
    , ( (3, -13)
      , ( Text.Label
          ( NounPhrasal.Noun
            Noun.Brewery
            []
            ( Determiner.Article
              True
              NumberParticle.Singular
            )
            Nothing
          )
        )
      )
    ]
  )


items : Dict ( number, number1 ) ( Item, number2 )
items =
  ( Dict.fromList
    [ ( ( 19, -23)
      , ( Item.Apple, 1 )
      )
    , ( ( 15, -24)
      , ( Item.Apple, 1 )
      )
    , ( ( 14, -26)
      , ( Item.Apple, 1 )
      )
    , ( ( 16, -22)
      , ( Item.Apple, 1 )
      )
    , ( ( 18, -26)
      , ( Item.Apple, 1 )
      )
    , ( ( 7, -27)
      , ( Item.Apple, 1 )
      )
    , ( ( 11, -22)
      , ( Item.Apple, 1 )
      )
    , ( ( 7, -24)
      , ( Item.Apple, 1 )
      )
    , ( ( 2, -29)
      , ( Item.Apple, 1 )
      )
    , ( ( 25, -21)
      , ( Item.Apple, 1 )
      )
    , ( ( 10, -10)
      , ( Item.Mushroom, 1 )
      )
    , ( ( 8, -6)
      , ( Item.Mushroom, 1 )
      )
    , ( ( 11, -18)
      , ( Item.Mushroom, 1 )
      )
    ]
  )


decorations : Dict ( number, number1 ) Decoration
decorations =
        ( Dict.fromList
          [ ( ( 4, -6)
            , Decoration
              { fgColor =
                0x964B00
              , bgColor =
                0x000000
              , tile =
                ( 7, 10 )
              }
              False
            )
          , ( ( 5, -6)
            , Decoration
              { fgColor =
                0xEFEFEF
              , bgColor =
                0x000000
              , tile =
                ( 5, 26 )
              }
              False
            )
          , ( ( 16, -25)
            , Decoration
              { fgColor =
                0x996600
              , bgColor =
                0x000000
              , tile =
                ( 2, 15 )
              }
              True
            )
          , ( ( 10, -24)
            , Decoration
              { fgColor =
                0x996600
              , bgColor =
                0x000000
              , tile =
                ( 2, 15 )
              }
              True
            )
          , ( ( 23, -22)
            , Decoration
              { fgColor =
                0x996600
              , bgColor =
                0x000000
              , tile =
                ( 2, 15 )
              }
              True
            )
          , ( ( 6, -30)
            , Decoration
              { fgColor =
                0x996600
              , bgColor =
                0x000000
              , tile =
                ( 2, 15 )
              }
              True
            )
          , ( ( 4, -27)
            , Decoration
              { fgColor =
                0x996600
              , bgColor =
                0x000000
              , tile =
                ( 2, 15 )
              }
              True
            )
          , ( ( 0, -18)
            , Decoration
              { fgColor =
                0x996600
              , bgColor =
                0x000000
              , tile =
                ( 7, 16 )
              }
              True
            )
          , ( ( -1, -18)
            , Decoration
              { fgColor =
                0x996600
              , bgColor =
                0x000000
              , tile =
                ( 7, 16 )
              }
              True
            )
          , ( ( -2, -18)
            , Decoration
              { fgColor =
                0x996600
              , bgColor =
                0x000000
              , tile =
                ( 7, 16 )
              }
              True
            )
          , ( ( -3, -18)
            , Decoration
              { fgColor =
                0x996600
              , bgColor =
                0x000000
              , tile =
                ( 7, 16 )
              }
              True
            )
          , ( ( 10, -13)
            , Decoration
              { fgColor =
                0x996600
              , bgColor =
                0x000000
              , tile =
                ( 7, 9 )
              }
              True
            )
          , ( ( 13, -28 )
            , Decoration
              { fgColor =
                0x996600
              , bgColor =
                0x000000
              , tile =
                ( 0, 22 )
              }
              True
            )
          , ( ( 12, -28 )
            , Decoration
              { fgColor =
                0x996600
              , bgColor =
                0x000000
              , tile =
                ( 0, 22 )
              }
              True
            )
          , ( ( 11, -28 )
            , Decoration
              { fgColor =
                0x996600
              , bgColor =
                0x000000
              , tile =
                ( 0, 22 )
              }
              True
            )
          , ( ( 12, -32 )
            , Decoration
              { fgColor =
                0x9F7F4F
              , bgColor =
                0x000000
              , tile =
                ( 5, 31 )
              }
              True
            )
          , ( ( 11, -32 )
            , Decoration
              { fgColor =
                0x9F7F4F
              , bgColor =
                0x000000
              , tile =
                ( 6, 4 )
              }
              True
            )
          , ( ( 10, -32 )
            , Decoration
              { fgColor =
                0x9F7F4F
              , bgColor =
                0x000000
              , tile =
                ( 6, 26 )
              }
              True
            )
          , ( ( 11, -31 )
            , Decoration
              { fgColor =
                0x9F7F4F
              , bgColor =
                0x000000
              , tile =
                ( 7, 3 )
              }
              False
            )
          ]
        )


init : a -> ( Model, Cmd Message )
init _ =
  ( { generatedTileData =
      Nothing
    , nextFrame =
      1
    , currentFrame =
      0
    , tileAtlas =
      Nothing
    , ipaAtlas =
      Nothing
    , language =
      Nothing
    , seed =
      Nothing
    , map =
      Map.map
        (0, 0)
        rooms
        npcs
        signs
        items
        decorations
    }
  , Cmd.batch
      [ Curses.loadTileAtlas
          { rows =
            16
          , cols =
            32
          }
          "assets/tile-atlas.png"
        |> Task.attempt Message.TileAtlasLoaded
      , Random.independentSeed
        |> Random.map (Random.step Language.generate)
        |> Random.generate Message.LanguageAndSeedGenerated
      ]
  )

