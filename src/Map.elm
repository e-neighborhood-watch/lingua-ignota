module Map exposing
  ( Map
  , map
  , render
  , movePlayer
  , updateQuest
  )


import Array exposing
  ( Array
  )
import Curses.Test3 as Curses
import DialogResponse exposing
  ( DialogResponse (..)
  )
import Extra.List as List
import Extra.String as String
import Language exposing
  ( Language
  )
import Language.Orthography as Orthography
import Language.Sentence as Sentence exposing
  ( Sentence
  )
import Map.Decoration as Decoration exposing
  ( Decoration (..)
  )
import Map.Inventory as Inventory exposing
  ( Inventory
  )
import Map.Item as Item exposing
  ( Item
  )
import Map.NonPlayerCharacter as NonPlayerCharacter exposing
  ( NonPlayerCharacter
  )
import Map.Room as Room exposing
  ( Room
  )
import Map.Room.Tile as RoomTile
import Map.Sign as Sign exposing
  ( Sign
  )
import Map.Text as Text exposing
  ( Text
  )
import Random
import Quest exposing
  ( Quest
  )
import Dict exposing
  ( Dict
  )


type Map =
  Map
    { rooms :
      List Room
    , playerX :
      Int
    , playerY :
      Int
    , npcs :
      List NonPlayerCharacter
    , signs :
      Dict (Int, Int) Sign
    , decorations :
      Dict (Int, Int) Decoration
    , items :
      Dict (Int, Int) (Item, Int)
    , dialog :
      Text
    , dialogQuestNPC :
      Maybe NonPlayerCharacter
    , playerInventory
      : Inventory
    }


map :
  (Int, Int)
  -> List Room
  -> List NonPlayerCharacter
  -> Dict (Int, Int) Sign
  -> Dict (Int, Int) (Item, Int)
  -> Dict (Int, Int) Decoration
  -> Map
map (pX, pY) rooms npcs signs items decos =
  Map
    { rooms =
      rooms
    , playerX =
      pX
    , playerY =
      pY
    , npcs =
      npcs
    , signs =
      signs
    , items =
      items
    , decorations =
      decos
    , dialog =
      Text.Sentences []
    , dialogQuestNPC =
      Nothing
    , playerInventory =
      Inventory.empty
    }


allNPCs : Map -> List NonPlayerCharacter
allNPCs (Map theMap) =
  case
    theMap.dialogQuestNPC
  of
    Nothing ->
      theMap.npcs
    Just questNPC ->
      questNPC :: theMap.npcs


updateQuest : DialogResponse -> Map -> Random.Generator Map
updateQuest response (Map oldMap) =
  case
    response
  of
    No ->
      { oldMap
      | dialogQuestNPC =
        Nothing
      , npcs =
        Map oldMap
        |> allNPCs
      , dialog =
        Text.Sentences []
      }
      |> Map
      |> Random.constant
    Yes ->
      case
        oldMap.dialogQuestNPC
      of
        Nothing ->
          oldMap
          |> Map
          |> Random.constant
        Just questGiver ->
          case
            NonPlayerCharacter.getQuest questGiver
          of
            Nothing ->
              oldMap
              |> Map
              |> Random.constant
            Just quest ->
              if
                Quest.isComplete quest.quest oldMap.playerInventory
              then
                Quest.generate
                |>
                  Random.map
                    ( \ newQuest ->
                        { oldMap
                        | playerInventory =
                          Quest.complete quest.quest oldMap.playerInventory
                        , dialogQuestNPC =
                          Nothing
                        , npcs =
                          NonPlayerCharacter.setQuest
                            { quest =
                              newQuest
                            , overrideText =
                              Nothing
                            }
                            questGiver
                          :: oldMap.npcs
                        , dialog =
                          Text.Sentences []
                        }
                        |> Map
                    )
              else
                oldMap
                |> Map
                |> Random.constant


movePlayer : Int -> Int -> Map -> Maybe Map
movePlayer dx dy (Map prev) =
  if
    dx == 0
    && dy == 0
  then
    Nothing
  else
    let
      newX : Int
      newX = prev.playerX + dx

      newY : Int
      newY = prev.playerY + dy
    in
      case
        Room.layer newX newY (newX, newY) prev.rooms
      of
        RoomTile.Wall _ ->
          Nothing
        _ ->
          case
            Map prev
            |> allNPCs
            |> List.delete (NonPlayerCharacter.at newX newY)
          of
            Nothing ->
              case
                Dict.get (newX, newY) prev.signs
              of
                Nothing ->
                  case
                    Dict.get (newX, newY) prev.decorations
                  of
                    Just (Decoration _ True) ->
                      Nothing
                    _ ->
                      case
                        Dict.get (newX, newY) prev.items
                      of
                        Nothing ->
                          { prev
                          | playerX =
                            newX
                          , playerY =
                            newY
                          , dialog =
                            Text.Sentences []
                          , dialogQuestNPC =
                            Nothing
                          , npcs =
                            Map prev
                            |> allNPCs
                          }
                          |> Map
                          |> Just
                        Just (item, count) ->
                          { prev
                          | playerX =
                            newX
                          , playerY =
                            newY
                          , items =
                            Dict.remove
                              (newX, newY)
                              prev.items
                          , playerInventory =
                            Inventory.add
                              item
                              count
                              prev.playerInventory
                          , dialog =
                            Inventory.pickUpMessage item count
                            |> List.singleton
                            |> Text.Sentences
                          , dialogQuestNPC =
                            Nothing
                          , npcs =
                            Map prev
                            |> allNPCs
                          }
                          |> Map
                          |> Just
                Just sign ->
                  if
                    sign
                    == prev.dialog
                  then
                    Nothing
                  else
                    { prev
                    | dialog =
                      sign
                    , dialogQuestNPC =
                      Nothing
                    , npcs =
                      Map prev
                      |> allNPCs
                    }
                    |> Map
                    |> Just
            Just ( npc, restOfNPCs ) ->
              let
                speech : Text
                speech =
                  NonPlayerCharacter.listen prev.playerInventory npc
              in
                if
                  speech
                  == prev.dialog
                then
                  Nothing
                else
                  { prev
                  | dialog = speech
                  , dialogQuestNPC =
                    Just npc
                  , npcs = restOfNPCs
                  }
                  |> Map
                  |> Just


render : Int -> Int -> Maybe Language -> Map -> Array Curses.Tile
render rows cols maybeLanguage (Map theMap) =
  let
    nullRenderer : Int -> Int -> Curses.Tile
    nullRenderer =
      { fgColor = 0x000000
      , bgColor = 0x000000
      , tile = ( 0, 0 )
      }
      |> always
      |> always

    textRenderer : Maybe (Int -> Int -> Curses.Tile)
    textRenderer =
      case
        maybeLanguage
      of
        Nothing ->
          Nothing
        Just language ->
          case
            theMap.dialog
          of
            Text.Sentences [] ->
              Nothing
            _ ->
              Text.render 5 (cols - 2) language theMap.dialog
              |> Just


    inventoryRenderer : Int -> Int -> Curses.Tile
    inventoryRenderer =
      case
        maybeLanguage
      of
        Nothing ->
          nullRenderer
        Just language ->
          Inventory.render 10 (cols - 2) language theMap.playerInventory


    mapRenderer : Int -> Int -> Curses.Tile
    mapRenderer row col =
      renderMap
        (Map theMap)
        (col - cols // 2 + theMap.playerX)
        (row - (rows - 6) // 2 + theMap.playerY)


    questComplete : Bool
    questComplete =
      case
        Maybe.andThen
          NonPlayerCharacter.getQuest
          theMap.dialogQuestNPC
      of
        Nothing ->
          False
        Just quest ->
          Quest.isComplete quest.quest theMap.playerInventory

    go : Int -> Curses.Tile
    go ix =
      let
        row : Int
        row = ix // cols

        col : Int
        col = modBy cols ix
      in
        if
          0 < col
          && col < cols - 1
        then
          if
            0 < row
            && row < rows - 17
          then
            mapRenderer row col
          else
            if
              rows - 17 < row
              && row < rows - 11
            then
              case
                textRenderer
                |> Maybe.map ((|>) (row - rows + 16) >> (|>) (col - 1))
              of
                Nothing ->
                  mapRenderer row col
                Just tile ->
                  tile
            else
              if
                row == rows - 17
                && textRenderer == Nothing
              then
                mapRenderer row col
              else
                if
                  rows - 11 < row
                  && row < rows - 1
                then
                  inventoryRenderer
                    ( row - rows + 10 )
                    ( col - 1 )
                else
                  if
                    questComplete
                    && row == rows - 11
                  then
                    if
                      col == 2
                    then
                      { fgColor = 0x00FF00
                      , bgColor = 0x000000
                      , tile = Text.charToTile 'Y'
                      }
                    else
                      if
                        col == 3
                      then
                        { fgColor = 0xEFEFEF
                        , bgColor = 0x000000
                        , tile = Text.charToTile '/'
                        }
                      else
                        if
                          col == 4
                        then
                          { fgColor = 0xFF0000
                          , bgColor = 0x000000
                          , tile = Text.charToTile 'N'
                          }
                        else
                          { fgColor = 0xFFFFFF
                          , bgColor = 0x000000
                          , tile = ( 6, 4 )
                          }
                  else
                    { fgColor = 0xFFFFFF
                    , bgColor = 0x000000
                    , tile = ( 6, 4 )
                    }
        else
          { fgColor = 0xFFFFFF
          , bgColor = 0x000000
          , tile =
            if
              row == 0
            then
              if
                col == 0
              then
                ( 6, 26 )
              else
                ( 5, 31 )
            else
              if
                ( row == rows - 17
                  && textRenderer /= Nothing
                )
                || row == rows - 11
              then
                if
                  col == 0
                then
                  ( 6, 3 )
                else
                  ( 5, 20 )
              else
                if
                  row == rows - 1
                then
                  if
                    col == 0
                  then
                    ( 6, 0 )
                  else
                    ( 6, 25 )
                else
                  ( 5, 19 )
          }
  in
    Array.initialize (rows*cols) go


renderMap : Map -> Int -> Int -> Curses.Tile
renderMap (Map theMap) x y =
  if
    x == theMap.playerX
    && y == theMap.playerY
  then
    { fgColor = 0xFF0000
    , bgColor = 0x000000
    , tile = ( 2, 0 )
    }
  else
    let
      roomTile : RoomTile.Tile
      roomTile = Room.layer x y ( theMap.playerX, theMap.playerY ) theMap.rooms
    in
      case
        roomTile
      of
        RoomTile.Hidden ->
          RoomTile.render RoomTile.Hidden
        _ ->
          case
            Map theMap
            |> allNPCs
            |> List.find (NonPlayerCharacter.at x y)
          of
            Just npc ->
              NonPlayerCharacter.render npc
            Nothing ->
              case
                Dict.get (x, y) theMap.items
              of
                Just (item, _) ->
                  Item.render item
                Nothing ->
                  case
                    Dict.get (x, y) theMap.decorations
                  of
                    Just (Decoration tile _) ->
                      tile
                    Nothing ->
                      case
                        Dict.get (x, y) theMap.signs
                      of
                        Nothing ->
                          RoomTile.render roomTile
                        Just sign ->
                          Sign.render sign
